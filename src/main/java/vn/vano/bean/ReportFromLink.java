package vn.vano.bean;

public class ReportFromLink {
    String dateReport;
    String linkReq;
    Integer numReg;
    String numCancel;
    String active;
    String numGh;
    String numRate;
    String amountGh;
    String amountTatol;

    public String getDateReport() {
        return dateReport;
    }

    public void setDateReport(String dateReport) {
        this.dateReport = dateReport;
    }

    public String getLinkReq() {
        return linkReq;
    }

    public void setLinkReq(String linkReq) {
        this.linkReq = linkReq;
    }

    public Integer getNumReg() {
        return numReg;
    }

    public void setNumReg(Integer numReg) {
        this.numReg = numReg;
    }

    public String getNumCancel() {
        return numCancel;
    }

    public void setNumCancel(String numCancel) {
        this.numCancel = numCancel;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getNumGh() {
        return numGh;
    }

    public void setNumGh(String numGh) {
        this.numGh = numGh;
    }

    public String getNumRate() {
        return numRate;
    }

    public void setNumRate(String numRate) {
        this.numRate = numRate;
    }

    public String getAmountGh() {
        return amountGh;
    }

    public void setAmountGh(String amountGh) {
        this.amountGh = amountGh;
    }

    public String getAmountTatol() {
        return amountTatol;
    }

    public void setAmountTatol(String amountTatol) {
        this.amountTatol = amountTatol;
    }


}
