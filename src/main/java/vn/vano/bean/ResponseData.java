package vn.vano.bean;

import vn.vano.common.IGsonBase;

public class ResponseData implements IGsonBase {
    private String code = "1";
    private String message = "";

    @Override
    public String toString() {
        return GSON.toJson(this);
    }
}
