package vn.vano.controller;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import vn.vano.bean.LinkRegBean;
import vn.vano.bean.LinkUserBean;
import vn.vano.common.Common;
import vn.vano.common.Constants;
import vn.vano.jpa.*;
import vn.vano.service.CommonService;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Controller
@RequestMapping(value = "")
public class ReportRevenueController {
    private final Logger LOG = LoggerFactory.getLogger(ReportRevenueController.class);

    @Autowired
    private CommonService commonService;


    /**
     * Bao cao san luong theo ngay
     *
     * @param request
     * @param model
     * @param pageable
     * @return
     */
    @RequestMapping(value = "/report/quantity-day.html", method = {RequestMethod.GET, RequestMethod.POST})
    public String indexQuantityDay(HttpServletRequest request, Model model, Pageable pageable,
                                   @ModelAttribute("fromObj") QuantityDayBean frm) {
        String transId = Common.randomString(Constants.TRANS_ID_LENGTH);
        LOG.debug(transId + "::BEGIN::quantity-day.html");
        DateTime dtCurrent = new DateTime();
        try {

            request.getSession().removeAttribute("fromObj");

            DateTime jodaDate = new DateTime();
            String strFromDate = frm.getFromDateStr();
            String strToDate = frm.getToDateStr();
            if (Common.isBlank(strFromDate)) {
                strFromDate = Common.dateToString(dtCurrent.minusDays(6).toDate(), Common.DATE_DDMMYYYY);
            }

            if (Common.isBlank(strToDate)) {
                strToDate = Common.dateToString(dtCurrent.toDate(), Common.DATE_DDMMYYYY);
            }

            initQuantityDayOnline(transId, model, pageable, strFromDate, strToDate);

            frm.setFromDateStr(strFromDate);
            frm.setToDateStr(strToDate);
            model.addAttribute("fromObj", frm);
            request.getSession().setAttribute("fromObj", frm);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.debug(transId + "::END::quantity-day.html");
        return "/report/quantity-day";
    }

    /**
     * Bao cao doanh thu theo date_report
     * @param request
     * @param model
     * @param pageable
     * @return
     */
    @RequestMapping(value = "/report/revenue-day.html", method = {RequestMethod.GET, RequestMethod.POST})
    public String indexRevenueDay(HttpServletRequest request, Model model, Pageable pageable,
                                  @ModelAttribute("fromObj") RevenueDayBean frm) {
        String transId = Common.randomString(Constants.TRANS_ID_LENGTH);
        LOG.debug(transId + "::BEGIN::revenue-day.html");
        DateTime dtCurrent = new DateTime();
        try {
            request.getSession().removeAttribute("fromObj");

            DateTime jodaDate = new DateTime();
            String strFromDate = frm.getFromDateStr();
            String strToDate = frm.getToDateStr();
            if (Common.isBlank(strFromDate)) {
                strFromDate = Common.dateToString(dtCurrent.minusDays(6).toDate(), Common.DATE_DDMMYYYY);
            }

            if (Common.isBlank(strToDate)) {
                strToDate = Common.dateToString(dtCurrent.toDate(), Common.DATE_DDMMYYYY);
            }

            initDataRevenueDay(transId, model, pageable, strFromDate, strToDate);

            frm.setFromDateStr(strFromDate);
            frm.setToDateStr(strToDate);
            model.addAttribute("fromObj", frm);
            request.getSession().setAttribute("fromObj", frm);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.debug(transId + "::END::revenue-day.html");
        return "/report/revenue-day";
    }


    /**
     * Bao cao doanh thu theo date_report
     * @param request
     * @param model
     * @param pageable
     * @return
     */
    @RequestMapping(value = "/report/fromlink-day.html", method = {RequestMethod.GET, RequestMethod.POST})
    public String indexFromLinkDay(HttpServletRequest request, Model model, Pageable pageable,
                                  @ModelAttribute("fromObj") ReportFromlinkDayly frm) {
        String transId = Common.randomString(Constants.TRANS_ID_LENGTH);
        LOG.debug(transId + "::BEGIN::fromlink-day.html");
        DateTime dtCurrent = new DateTime();
        try {
            request.getSession().removeAttribute("fromObj");

            DateTime jodaDate = new DateTime();
            String strFromDate = frm.getFromDateStr();
            String strToDate = frm.getToDateStr();
            String LinkRegTT = frm.getLinkReqTT();
            String userTT = frm.getUserTT();
            if (Common.isBlank(strFromDate)) {
                strFromDate = Common.dateToString(dtCurrent.minusDays(6).toDate(), Common.DATE_DDMMYYYY);
            }

            if (Common.isBlank(strToDate)) {
                strToDate = Common.dateToString(dtCurrent.toDate(), Common.DATE_DDMMYYYY);
            }

            if (Common.isBlank(userTT)) {
                userTT = "ALL";
            }
            String lstUser = getLinkByKey(transId,"USER_TT");
            String lstuserTT []= lstUser.split(";");
            List<LinkUserBean> LstUserBean = new ArrayList<>();
            for (String a : lstuserTT){
                LinkUserBean userBean = new LinkUserBean();
                userBean.setUserTT(a);
                LstUserBean.add(userBean);
            }
            model.addAttribute("lstuserTT", LstUserBean);

            if (Common.isBlank(LinkRegTT)) {
                LinkRegTT = "ALL";
            }
            String lstLink = getLinkByKey(transId,userTT);
            initLinkReg(transId,model,"1",lstLink);

            initDataReportFromLink(transId, model, pageable, strFromDate, strToDate,LinkRegTT,lstLink);


            frm.setFromDateStr(strFromDate);
            frm.setToDateStr(strToDate);
            frm.setLinkReqTT(LinkRegTT);

            model.addAttribute("fromObj", frm);
            request.getSession().setAttribute("fromObj", frm);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.debug(transId + "::END::fromlink-day.html");
        return "/report/fromlink-day";
    }


    @RequestMapping(value = "/report/usserTT.html/{usserTT}", method = {RequestMethod.GET})
    public String unregPackage(HttpServletRequest request, Model model, Pageable pageable,
                               @PathVariable("usserTT") String userTT,
                               RedirectAttributes redirectAttributes) {
        String transId = Common.randomString(Constants.TRANS_ID_LENGTH);
        LOG.debug(transId + "::BEGIN::report/usserTT.htm");

        ReportFromlinkDayly frm = new ReportFromlinkDayly();
//        model.addAttribute("fromObj", frm);
        request.getSession().setAttribute("fromObj", frm);
//        String userTT = frm.getUserTT();
        if (Common.isBlank(userTT)) {
            userTT = "ALL";
        }
        String lstLink = getLinkByKey(transId,userTT);
        initLinkReg(transId,model,"1",lstLink);

        return "redirect:/report/fromlink-day.html";
    }


    @RequestMapping(value = "/reportUrl/fromlink-day1.html", method = {RequestMethod.GET, RequestMethod.POST})
    public String indexFromLinkDay1(HttpServletRequest request, Model model, Pageable pageable,
                                   @ModelAttribute("fromObj") ReportFromlinkDayly1 frm) {
        String transId = Common.randomString(Constants.TRANS_ID_LENGTH);
        LOG.debug(transId + "::BEGIN::fromlink-day1.html");
        DateTime dtCurrent = new DateTime();
        try {
            request.getSession().removeAttribute("fromObj");

            DateTime jodaDate = new DateTime();
            String strFromDate = frm.getFromDateStr();
            String strToDate = frm.getToDateStr();
            String LinkRegTT = frm.getLinkReqTT();
            String username = request.getSession().getAttribute("userName").toString();

            if (Common.isBlank(strFromDate)) {
                strFromDate = Common.dateToString(dtCurrent.minusDays(6).toDate(), Common.DATE_DDMMYYYY);
            }

            if (Common.isBlank(strToDate)) {
                strToDate = Common.dateToString(dtCurrent.toDate(), Common.DATE_DDMMYYYY);
            }


            if (Common.isBlank(LinkRegTT)) {
                LinkRegTT = "ALL";
            }
            String lstLink = getLinkByKey(transId,username.toLowerCase());
            initLinkReg(transId,model,"1",lstLink);

            initDataReportFromLink1(transId, model, pageable, strFromDate, strToDate,LinkRegTT,lstLink);


            frm.setFromDateStr(strFromDate);
            frm.setToDateStr(strToDate);
            frm.setLinkReqTT(LinkRegTT);

            model.addAttribute("fromObj", frm);
            request.getSession().setAttribute("fromObj", frm);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.debug(transId + "::END::fromlink-day1.html");
        return "/reportUrl/fromlink-day1";
    }



    /**
     * Bao cao san luong theo ngay link goc
     *
     * @param request
     * @param model
     * @param pageable
     * @return
     */
    @RequestMapping(value = "/reportWap/quantity-day-wap.html", method = {RequestMethod.GET, RequestMethod.POST})
    public String indexQuantityWap(HttpServletRequest request, Model model, Pageable pageable,
                                   @ModelAttribute("fromObj") QuantityWapBean frm) {
        String transId = Common.randomString(Constants.TRANS_ID_LENGTH);
        LOG.debug(transId + "::BEGIN::quantity-day-wap.html");
        DateTime dtCurrent = new DateTime();
        try {

            request.getSession().removeAttribute("fromObj");

            DateTime jodaDate = new DateTime();
            String strFromDate = frm.getFromDateStr();
            String strToDate = frm.getToDateStr();
            if (Common.isBlank(strFromDate)) {
                strFromDate = Common.dateToString(dtCurrent.minusDays(6).toDate(), Common.DATE_DDMMYYYY);
            }

            if (Common.isBlank(strToDate)) {
                strToDate = Common.dateToString(dtCurrent.toDate(), Common.DATE_DDMMYYYY);
            }

            initQuantityWapDayOnline(transId, model, pageable, strFromDate, strToDate);

            frm.setFromDateStr(strFromDate);
            frm.setToDateStr(strToDate);
            model.addAttribute("fromObj", frm);
            request.getSession().setAttribute("fromObj", frm);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.debug(transId + "::END::quantity-day-wap.html");
        return "/reportWap/quantity-day-wap";
    }

    @RequestMapping(value = "/reportWap/quantity-wap-chanel.html", method = {RequestMethod.GET, RequestMethod.POST})
//    @PreAuthorize("hasAnyAuthority('SADMIN','ADMIN','WAP_REPORT')")
    public String indexQuantityWapChanel(HttpServletRequest request, Model model, Pageable pageable,
                                   @ModelAttribute("fromObj") QuantityWapChanelBean frm) {
        String transId = Common.randomString(Constants.TRANS_ID_LENGTH);
        LOG.debug(transId + "::BEGIN::quantity-wap-chanel.html");
        DateTime dtCurrent = new DateTime();
        try {

            request.getSession().removeAttribute("fromObj");

            DateTime jodaDate = new DateTime();
            String strFromDate = frm.getFromDateStr();
            String strToDate = frm.getToDateStr();
            if (Common.isBlank(strFromDate)) {
                strFromDate = Common.dateToString(dtCurrent.minusDays(6).toDate(), Common.DATE_DDMMYYYY);
            }

            if (Common.isBlank(strToDate)) {
                strToDate = Common.dateToString(dtCurrent.toDate(), Common.DATE_DDMMYYYY);
            }

            initQuantityWapDayChanel(transId, model, pageable, strFromDate, strToDate);

            frm.setFromDateStr(strFromDate);
            frm.setToDateStr(strToDate);
            model.addAttribute("fromObj", frm);
            request.getSession().setAttribute("fromObj", frm);
        } catch (Exception ex) {
            LOG.error("", ex);
        }
        LOG.debug(transId + "::END::quantity-wap-chanel.html");
        return "/reportWap/quantity-wap-chanel";
    }

    private void initDataRevenueDay(String transId, Model model, Pageable pageable, String fromDate,String toDate) {
        Page<RevenueDayBean> pageData = null;
        Pageable _pageable = null;
        try {
            List<RevenueDayBean> lst = new ArrayList<>();

            DateTime jodaDate = new DateTime();
            Date dtMonth = null;
//            if (!Common.isBlank(month)) {
//                dtMonth = Common.strToDate(month, Common.FORM_DATE_MMYYYY);
//                month = Common.dateToString(dtMonth, Common.DATE_YYYYMMDD);
//            } else {
//                dtMonth = jodaDate.toDate();
//                month = Common.dateToString(dtMonth, Common.DATE_YYYYMMDD);
//            }
//           STR_TO_DATE(?,'%d/%m/%Y')
            _pageable = new PageRequest(pageable.getPageNumber(), 50, null);

            String[] params = new String[]{fromDate, toDate,fromDate, toDate};
            String sql = "select date_report,sum(amount_reg) amount_dk , sum(amount_gh) amount_renew, (sum(amount_reg) + sum(amount_gh)) amount_total from ( SELECT DATE(log_time) AS date_report , SUM(price) AS amount_reg , 0 amount_gh " +
                    " FROM log_notify where reg_type =1 AND log_time >= STR_TO_DATE(?,'%d/%m/%Y') and date(log_time) <= STR_TO_DATE(?,'%d/%m/%Y') " +
                    " GROUP BY DATE(log_time) " +
                    " UNION ALL " +
                    " SELECT DATE(action_date) AS date_report, 0 amount_reg , SUM(price) AS amount_gh " +
                    " FROM log_renew WHERE action_date >=  STR_TO_DATE(?,'%d/%m/%Y') and DATE(action_date) <= STR_TO_DATE(?,'%d/%m/%Y') GROUP BY DATE(action_date) ) t group by date_report order by date_report desc";
                    List<Object[]> lstObj = commonService.findBySQL(transId, sql, params);
            if(lstObj != null && !lstObj.isEmpty()) {
                lstObj.stream().forEach(item -> {
                    RevenueDayBean bean = new RevenueDayBean();
                    bean = Common.convertToBean(item, bean);
                    lst.add(bean);
                });
            }

            //Thuc hien phan trang cho danh sach
            int fromRecord = 0;
            int toRecord = 0;

            fromRecord = _pageable.getPageNumber() * _pageable.getPageSize();
            toRecord = (_pageable.getPageNumber() + 1) * _pageable.getPageSize();
            if(toRecord > lst.size()) {
                toRecord = lst.size();
            }

            List<RevenueDayBean> lstResult = null;
            if (lst != null && !lst.isEmpty()) {
                lstResult = lst.subList(fromRecord, toRecord);
            } else {
                lstResult = new ArrayList<>();
            }

            pageData = new PageImpl<>(lstResult, _pageable, lst.size());
        } catch (Exception ex) {
            pageData = new PageImpl<>(new ArrayList<RevenueDayBean>(), _pageable, 0);
        } finally {
            model.addAttribute("page", pageData);
        }
    }





    private void initQuantityDayOnline(String transId, Model model, Pageable pageable, String fromDate,String toDate) {
        Page<QuantityDayBean> pageData = null;
        Pageable _pageable = null;
        try {
            List<QuantityDayBean> lst = new ArrayList<>();

            DateTime jodaDate = new DateTime();
            Date dtMonth = null;
            _pageable = new PageRequest(pageable.getPageNumber(), 50, null);
            String[] params = new String[]{fromDate, toDate,fromDate, toDate,fromDate, toDate};

            String sql = "select ngay DATE_REPORT, sum(num_regnew) num_regnew,  sum(num_renew) num_renew, sum(num_dk) num_dk,sum(num_reg_api) num_reg_api,sum(num_reg_cskh) num_reg_cskh,sum(num_reg_ivr) num_reg_ivr,\n" +
                    "sum(num_reg_sms) num_reg_sms,sum(num_reg_ussd) num_reg_ussd,sum(num_reg_wap) num_reg_wap,sum(num_reg_web) num_reg_web,sum(num_huy_api) num_huy_api,sum(num_huy_cskh) num_huy_cskh,\n" +
                    "sum(num_huy_ivr) num_huy_ivr,sum(num_huy_sms) num_huy_sms,sum(num_huy_ussd) num_huy_ussd,sum(num_huy_wap) num_huy_wap,sum(num_huy_web) num_huy_web,sum(num_huy ) num_huy,\n" +
                    "sum(num_active) num_active, sum(num_gh) num_gh,  ROUND(100*SUM(num_gh)/SUM(num_active),2) num_rate  " +
                    "from (\n" +
                    "SELECT DATE(log_time) AS ngay ,sum(if(reg_type =1 and price =0 ,1,0)) num_regnew,sum(if(reg_type =1 and price >0 ,1,0)) num_renew , sum(if(reg_type =1 ,1,0)) num_dk, \n" +
                    "sum(if(reg_type =1 and channel = 'API',1,0)) num_reg_api, sum(if(reg_type =1 and channel = 'CSKH',1,0)) num_reg_cskh, sum(if(reg_type =1 and channel = 'IVR',1,0)) num_reg_ivr\n" +
                    ", sum(if(reg_type =1 and channel = 'SMS',1,0)) num_reg_sms, sum(if(reg_type =1 and channel = 'USSD',1,0)) num_reg_ussd, sum(if(reg_type =1 and channel = 'WAP',1,0)) num_reg_wap, sum(if(reg_type =1 and channel = 'WEB',1,0)) num_reg_web,\n" +
                    "sum(if(reg_type =2 and channel = 'API',1,0)) num_huy_api, sum(if(reg_type =2 and channel = 'CSKH',1,0)) num_huy_cskh, sum(if(reg_type =2 and channel = 'IVR',1,0)) num_huy_ivr\n" +
                    ", sum(if(reg_type =2 and channel = 'SMS',1,0)) num_huy_sms, sum(if(reg_type =2 and channel = 'USSD',1,0)) num_huy_ussd, sum(if(reg_type =2 and channel = 'WAP',1,0)) num_huy_wap, sum(if(reg_type =2 and channel = 'WEB',1,0)) num_huy_web,\n" +
                    "sum(if(reg_type =2 ,1,0)) num_huy,0 num_active, 0 num_gh \n" +
                    "FROM log_notify where  log_time >= STR_TO_DATE(?,'%d/%m/%Y') and date(log_time) <= STR_TO_DATE(?,'%d/%m/%Y')\n" +
                    "GROUP BY DATE(log_time) , reg_type\n" +
                    "UNION ALL \n" +
                    "SELECT DATE(ngay) AS ngay, 0 num_regnew, 0 num_renew, 0 num_dk,0 num_reg_api,0 num_reg_cskh,0 num_reg_ivr,0 num_reg_sms,0 num_reg_ussd,0 num_reg_wap,0 num_reg_web,\n" +
                    "0 num_huy_api,0 num_huy_cskh,0 num_huy_ivr,0 num_huy_sms,0 num_huy_ussd ,0 num_huy_wap,0 num_huy_web ,0 num_huy ,\n" +
                    "sum(soluong) AS num_active , 0 num_gh\t FROM rp_total_sub \t WHERE ngay >= STR_TO_DATE(?,'%d/%m/%Y') and date(ngay) <= STR_TO_DATE(?,'%d/%m/%Y') GROUP BY ngay\n" +
                    "UNION ALL \n" +
                    "SELECT DATE(action_date) AS ngay, 0 num_regnew, 0 num_renew, 0 num_dk,0 num_reg_api,0 num_reg_cskh,0 num_reg_ivr,0 num_reg_sms,0 num_reg_ussd,0 num_reg_wap,0 num_reg_web,\n" +
                    "0 num_huy_api,0 num_huy_cskh,0 num_huy_ivr,0 num_huy_sms,0 num_huy_ussd ,0 num_huy_wap,0 num_huy_web ,0 num_huy ,\n" +
                    "0 num_active , count(*) num_gh\n" +
                    " FROM log_renew WHERE action_date >=  STR_TO_DATE(?,'%d/%m/%Y') and date(action_date) <=  STR_TO_DATE(?,'%d/%m/%Y') group by DATE(action_date)\n " ;
//                    "UNION ALL " +
//                    "  SELECT CURRENT_DATE AS ngay, 0 num_regnew, 0 num_renew, 0 num_dk,0 num_reg_api,0 num_reg_cskh,0 num_reg_ivr,0 num_reg_sms,0 num_reg_ussd,0 num_reg_wap,0 num_reg_web, 0 num_huy_api,0 num_huy_cskh,0 num_huy_ivr,0 num_huy_sms,0 num_huy_ussd ,0 num_huy_wap,0 num_huy_web ,0 num_huy , count(*) num_active , 0 num_gh FROM subcriber  " +
            String strActive = " UNION ALL SELECT CURRENT_DATE AS ngay, 0 num_regnew, 0 num_renew, 0 num_dk,0 num_reg_api,0 num_reg_cskh,0 num_reg_ivr,0 num_reg_sms,0 num_reg_ussd,0 num_reg_wap,0 num_reg_web, 0 num_huy_api,0 num_huy_cskh,0 num_huy_ivr,0 num_huy_sms,0 num_huy_ussd ,0 num_huy_wap,0 num_huy_web ,0 num_huy , count(*) num_active , 0 num_gh FROM subcriber  " ;
            if (toDate.equalsIgnoreCase(Common.dateToString(jodaDate.toDate(), Common.DATE_DDMMYYYY))){
                sql = sql + strActive;
            }
            sql = sql +  " ) t group by ngay order by ngay desc" ;

            List<Object[]> lstObj = commonService.findBySQL(transId, sql, params);
            if(lstObj != null && !lstObj.isEmpty()) {
                lstObj.stream().forEach(item -> {
                    QuantityDayBean bean = new QuantityDayBean();
                    bean = Common.convertToBean(item, bean);

                    lst.add(bean);
                });
            }

            //Thuc hien phan trang cho danh sach
            int fromRecord = 0;
            int toRecord = 0;

            fromRecord = _pageable.getPageNumber() * _pageable.getPageSize();
            toRecord = (_pageable.getPageNumber() + 1) * _pageable.getPageSize();
            if(toRecord > lst.size()) {
                toRecord = lst.size();
            }

            List<QuantityDayBean> lstResult = null;
            if (lst != null && !lst.isEmpty()) {
                lstResult = lst.subList(fromRecord, toRecord);
            } else {
                lstResult = new ArrayList<>();
            }

            pageData = new PageImpl<>(lstResult, _pageable, lst.size());
        } catch (Exception ex) {
            pageData = new PageImpl<>(new ArrayList<QuantityDayBean>(), _pageable, 0);
        } finally {
            model.addAttribute("page", pageData);
        }
    }



    private void initQuantityWapDayOnline(String transId, Model model, Pageable pageable, String fromDate,String toDate) {
        Page<QuantityWapBean> pageData = null;
        Pageable _pageable = null;
        try {
            List<QuantityWapBean> lst = new ArrayList<>();

            DateTime jodaDate = new DateTime();
            Date dtMonth = null;
            _pageable = new PageRequest(pageable.getPageNumber(), 50, null);
            String[] params = new String[]{fromDate, toDate,fromDate, toDate,fromDate, toDate};

            String sql = "select DATE_REPORT, sum(num_reg - num_reg_link) num_reg,sum(num_reg_api) num_reg_api,sum(num_reg_cskh) num_reg_cskh,sum(num_reg_ivr) num_reg_ivr,  " +
                    " sum(num_reg_sms) num_reg_sms,sum(num_reg_ussd) num_reg_ussd, sum(num_reg - num_reg_link) num_reg_wap, " +
                    " sum(num_reg_sms) num_reg_web,sum(num_huy_api) num_huy_api,sum(num_huy_cskh) num_huy_cskh,sum(num_huy_ivr) num_huy_ivr,sum(num_huy_sms) num_huy_sms, " +
                    " sum(num_huy_ussd) num_huy_ussd,sum(num_huy_wap) num_huy_wap,sum(num_huy_web) num_huy_web,sum(num_huy) num_huy " +
                    " from " +
                    " (SELECT DATE(log_time) AS DATE_REPORT , sum(if(reg_type =1 ,1,0)) num_reg,0 num_reg_link, " +
                    " 0 num_reg_api, 0 num_reg_cskh, 0 num_reg_ivr,0 num_reg_sms, 0 num_reg_ussd, 0 num_reg_wap, 0 num_reg_web, " +
                    " 0 num_huy_api, 0 num_huy_cskh, 0 num_huy_ivr, " +
                    " 0 num_huy_sms, 0 num_huy_ussd, 0 num_huy_wap, 0 num_huy_web,0 num_huy " +
                    " FROM log_notify a where log_time >= '2021-09-01' and channel = 'WAP'  " +
                    " GROUP BY DATE(log_time)  " +
                    " UNION ALL " +
                    " select action_date AS DATE_REPORT ,0 num_reg, count(DISTINCT msisdn) num_reg_link,  " +
                    " 0 num_reg_api, 0 num_reg_cskh, 0 num_reg_ivr,0 num_reg_sms, 0 num_reg_ussd, 0 num_reg_wap, 0 num_reg_web, " +
                    " 0 num_huy_api, 0 num_huy_cskh, 0 num_huy_ivr, " +
                    " 0 num_huy_sms, 0 num_huy_ussd, 0 num_huy_wap, 0 num_huy_web,0 num_huy " +
                    " from request_logs_temp where action_date >= '2021-09-01' GROUP BY action_date " +
                    ") t GROUP BY DATE_REPORT " ;

            List<Object[]> lstObj = commonService.findBySQL(transId, sql, null);


            if(lstObj != null && !lstObj.isEmpty()) {
                String ulr = "http://vnpt.mytalk.vn/api/check-sub-reg" ;
                HttpResponse<String> response = Unirest.get(ulr)
                        .asString();
                Long numRegLink = 0L;
                DateTime dtCurrent = new DateTime();
                String strDate = Common.dateToString(dtCurrent.toDate(), Common.DATE_YYYY_MM_DD);

                if (response != null ){
                    String str = response.getBody().toString().replace("","");
                    numRegLink =  new Long(str).longValue(); //Long.valueOf(str.trim()).longValue();
                }
                final Long temp = numRegLink;
                AtomicReference<String> checkOnl = new AtomicReference<>("0");
                lstObj.stream().forEach(item -> {
                    QuantityWapBean bean = new QuantityWapBean();
                    bean = Common.convertToBean(item, bean);

                    if(bean.getDateReport().equalsIgnoreCase(strDate)){
                        bean.setNumRegWap(bean.getNumRegWap() - temp );
                        bean.setNumRegAll(bean.getNumRegAll() - temp );
                        checkOnl.set("1");
                    }
                    lst.add(bean);
                });
                if(checkOnl.get().equalsIgnoreCase("0")){
                    QuantityWapBean bean = new QuantityWapBean();
                    bean.setDateReport(strDate);
                    lst.add(bean);
                }


            }
// http://vnpt.mytalk.vn/api/check-sub-reg

            //Thuc hien phan trang cho danh sach
            int fromRecord = 0;
            int toRecord = 0;

            fromRecord = _pageable.getPageNumber() * _pageable.getPageSize();
            toRecord = (_pageable.getPageNumber() + 1) * _pageable.getPageSize();
            if(toRecord > lst.size()) {
                toRecord = lst.size();
            }

            List<QuantityWapBean> lstResult = null;
            if (lst != null && !lst.isEmpty()) {
                lstResult = lst.subList(fromRecord, toRecord);
            } else {
                lstResult = new ArrayList<>();
            }

            pageData = new PageImpl<>(lstResult, _pageable, lst.size());
        } catch (Exception ex) {
            pageData = new PageImpl<>(new ArrayList<QuantityWapBean>(), _pageable, 0);
        } finally {
            model.addAttribute("page", pageData);
        }
    }


    private void initQuantityWapDayChanel(String transId, Model model, Pageable pageable, String fromDate,String toDate) {
        Page<QuantityWapChanelBean> pageData = null;
        Pageable _pageable = null;
        try {
            List<QuantityWapChanelBean> lst = new ArrayList<>();

            DateTime jodaDate = new DateTime();
            Date dtMonth = null;
            _pageable = new PageRequest(pageable.getPageNumber(), 50, null);
            String[] params = new String[]{fromDate, toDate};

            String sql = "SELECT DATE(log_time) AS DATE_REPORT , count(1) num_reg " +
                    " FROM log_notify a where reg_type =1 and log_time >= '2021-10-28' and channel = 'WAP' and log_time >=  STR_TO_DATE(?,'%d/%m/%Y') and log_time < DATE_ADD(STR_TO_DATE(?,'%d/%m/%Y'),INTERVAL 1 DAY)  " +
                    " GROUP BY DATE(log_time)  ORDER BY DATE(log_time)  DESC " ;

            List<Object[]> lstObj = commonService.findBySQL(transId, sql, params);


            if(lstObj != null && !lstObj.isEmpty()) {
                lstObj.stream().forEach(item -> {
                    QuantityWapChanelBean bean = new QuantityWapChanelBean();
                    bean = Common.convertToBean(item, bean);
                    lst.add(bean);
                });


            }

            //Thuc hien phan trang cho danh sach
            int fromRecord = 0;
            int toRecord = 0;

            fromRecord = _pageable.getPageNumber() * _pageable.getPageSize();
            toRecord = (_pageable.getPageNumber() + 1) * _pageable.getPageSize();
            if(toRecord > lst.size()) {
                toRecord = lst.size();
            }

            List<QuantityWapChanelBean> lstResult = null;
            if (lst != null && !lst.isEmpty()) {
                lstResult = lst.subList(fromRecord, toRecord);
            } else {
                lstResult = new ArrayList<>();
            }

            pageData = new PageImpl<>(lstResult, _pageable, lst.size());
        } catch (Exception ex) {
            pageData = new PageImpl<>(new ArrayList<QuantityWapChanelBean>(), _pageable, 0);
        } finally {
            model.addAttribute("page", pageData);
        }
    }


    private void initDataReportFromLink(String transId, Model model, Pageable pageable, String fromDate,String toDate, String linkReg, String lstLink) {
        Page<ReportFromlinkDayly1> pageData = null;
        Pageable _pageable = null;
        try {
            List<ReportFromlinkDayly1> lst = new ArrayList<>();

            DateTime jodaDate = new DateTime();
            Date dtMonth = null;
//            if (!Common.isBlank(month)) {
//                dtMonth = Common.strToDate(month, Common.FORM_DATE_MMYYYY);
//                month = Common.dateToString(dtMonth, Common.DATE_YYYYMMDD);
//            } else {
//                dtMonth = jodaDate.toDate();
//                month = Common.dateToString(dtMonth, Common.DATE_YYYYMMDD);
//            }
//           STR_TO_DATE(?,'%d/%m/%Y')
            _pageable = new PageRequest(pageable.getPageNumber(), 50, null);
            String[] params = new String[]{fromDate, toDate};
            String sql = "select * from report_fromlink_dayly where date_report >= STR_TO_DATE(?,'%d/%m/%Y') and date_report < STR_TO_DATE(?,'%d/%m/%Y')" ;
//


                sql =  "select date_report,sum(num_reg) num_reg,sum(num_cancel) num_cancel,sum(active) active,sum(num_gh) num_gh,sum(amount_gh) amount_gh,sum(amount_total) amount_total, round(100*sum(num_gh)/sum(active),2) num_rate " +
                        " from report_fromlink_dayly where date_report >= STR_TO_DATE(?,'%d/%m/%Y') and date_report < DATE_ADD( STR_TO_DATE(?,'%d/%m/%Y'),INTERVAL 1 DAY) " ;


            if(!Common.isBlank(linkReg) && !linkReg.equalsIgnoreCase("ALL")){
                sql = sql+ " and link_req = ? ";
                params =  new String[]{fromDate, toDate,linkReg};
            }else if (!Common.isBlank(linkReg) && !Common.isBlank(lstLink) ) {
                sql = sql+ "  and link_req in " + lstLink;
            }

            sql = sql+  " group by date_report  order by date_report desc ";


            List<Object[]> lstObj = commonService.findBySQL(transId, sql, params);
            if(lstObj != null && !lstObj.isEmpty()) {
                lstObj.stream().forEach(item -> {
                    ReportFromlinkDayly1 bean = new ReportFromlinkDayly1();
                    bean = Common.convertToBean(item, bean);
                    lst.add(bean);
                });
            }

            //Thuc hien phan trang cho danh sach
            int fromRecord = 0;
            int toRecord = 0;

            fromRecord = _pageable.getPageNumber() * _pageable.getPageSize();
            toRecord = (_pageable.getPageNumber() + 1) * _pageable.getPageSize();
            if(toRecord > lst.size()) {
                toRecord = lst.size();
            }

            List<ReportFromlinkDayly1> lstResult = null;
            if (lst != null && !lst.isEmpty()) {
                lstResult = lst.subList(fromRecord, toRecord);
            } else {
                lstResult = new ArrayList<>();
            }

            pageData = new PageImpl<>(lstResult, _pageable, lst.size());
        } catch (Exception ex) {
            pageData = new PageImpl<>(new ArrayList<ReportFromlinkDayly1>(), _pageable, 0);
        } finally {
            model.addAttribute("page", pageData);
        }
    }


    private void initDataReportFromLink1(String transId, Model model, Pageable pageable, String fromDate,String toDate, String linkReg,String lstLink) {
        Page<ReportFromlinkDayly1> pageData = null;
        Pageable _pageable = null;
        try {
            List<ReportFromlinkDayly1> lst = new ArrayList<>();

            DateTime jodaDate = new DateTime();
            Date dtMonth = null;
            _pageable = new PageRequest(pageable.getPageNumber(), 50, null);
            String[] params = new String[]{fromDate, toDate};

            String sql =  "select date_report,sum(num_reg) num_reg,sum(num_cancel) num_cancel,sum(active) active,sum(num_gh) num_gh,sum(amount_gh) amount_gh,sum(amount_total) amount_total, round(100*sum(num_gh)/sum(active),2) num_rate " +
                    " from report_fromlink_dayly where date_report >= STR_TO_DATE(?,'%d/%m/%Y') and date_report < DATE_ADD( STR_TO_DATE(?,'%d/%m/%Y'),INTERVAL 1 DAY)  " ;

            if(!lstLink.equalsIgnoreCase("ALL")){ // neu khon phai user co quyen admin
                if(!Common.isBlank(linkReg) && !linkReg.equalsIgnoreCase("ALL")){
                    sql = sql+ " and link_req = ? ";
                    params =  new String[]{fromDate, toDate,linkReg};
                }else{
                    sql = sql+ "  and link_req in " + lstLink;
                }
            }else {
                if(!Common.isBlank(linkReg) && !linkReg.equalsIgnoreCase("ALL") ){
                    sql = sql+ " and link_req = ? ";
                    params =  new String[]{fromDate, toDate,linkReg};
                }
            }
//            else {
//                if(!Common.isBlank(linkReg) && !linkReg.equalsIgnoreCase("ALL")){
//                    sql = sql+ " and link_req = ? ";
//                    params =  new String[]{fromDate, toDate,linkReg};
//                }else if(!Common.isBlank(linkReg) && linkReg.equalsIgnoreCase("ALL")){
//                    sql = sql+ "  and link_req in " + lstLink;
//                }
//            }
            sql = sql+  " group by date_report  order by date_report desc ";


            List<Object[]> lstObj = commonService.findBySQL(transId, sql, params);
            if(lstObj != null && !lstObj.isEmpty()) {
                lstObj.stream().forEach(item -> {
                    ReportFromlinkDayly1 bean = new ReportFromlinkDayly1();
                    bean = Common.convertToBean(item, bean);
                    lst.add(bean);
                });
            }

            //Thuc hien phan trang cho danh sach
            int fromRecord = 0;
            int toRecord = 0;

            fromRecord = _pageable.getPageNumber() * _pageable.getPageSize();
            toRecord = (_pageable.getPageNumber() + 1) * _pageable.getPageSize();
            if(toRecord > lst.size()) {
                toRecord = lst.size();
            }

            List<ReportFromlinkDayly1> lstResult = null;
            if (lst != null && !lst.isEmpty()) {
                lstResult = lst.subList(fromRecord, toRecord);
            } else {
                lstResult = new ArrayList<>();
            }

            pageData = new PageImpl<>(lstResult, _pageable, lst.size());
        } catch (Exception ex) {
            pageData = new PageImpl<>(new ArrayList<ReportFromlinkDayly1>(), _pageable, 0);
        } finally {
            model.addAttribute("page", pageData);
        }
    }


    private  void initLinkReg(String transId, Model model, String typeReport,String lstLink) {
        List<Object[]> lstObj = null;
        try {
            String sql ="select distinct  link_req from  report_fromlink_dayly";
            if(!Common.isBlank(typeReport) && typeReport.equalsIgnoreCase("1")) {
                if (!Common.isBlank(lstLink) && !"ALL".equalsIgnoreCase(lstLink)) {
                    sql = sql + "  where link_req in  " + lstLink;
                }
            }
            lstObj = commonService.findBySQL(transId, sql, null);

            List<LinkRegBean> lstLinkRegBean = new ArrayList<>();
            if(lstObj != null && !lstObj.isEmpty()) {
                int intSize = lstObj.size();
                for (int i=0; i< intSize;i++){
                    LinkRegBean bean = new LinkRegBean();
                    bean.setLinkReqTT(String.valueOf(lstObj.get(i)));
                    lstLinkRegBean.add(bean);
                }
            }
//            return  lstLinkRegBean;
            model.addAttribute("lstLinkReq", lstLinkRegBean);
        } catch (Exception e) {
            LOG.error(transId, e);
        }
    }

    private String getLinkByKey(String transId, String key) {
        List<SysParam> lst = new ArrayList();
        try {
            List<Object[]> lstObj = commonService.findBySQL(transId, "SELECT * FROM sys_param WHERE _key = ? ", new String[]{key});
            if (!Common.isEmpty(lstObj)) {
                lstObj.stream().forEach(item -> {
                    SysParam sysParam = new SysParam();
                    sysParam = Common.convertToBean(item, sysParam);
                    lst.add(sysParam);
                });
            }
            if (!Common.isEmpty(lst)) {
                return lst.get(0).getValue();
            }
        } catch (Exception ex) {
            LOG.error(transId, ex);
        }
        return "";
    }

}
