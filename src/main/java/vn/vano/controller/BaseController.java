package vn.vano.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;
import vn.vano.common.Common;
import vn.vano.common.Constants;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

@Controller
public class BaseController {

    @Value("${pathFileCertificate}")
    public String pathFileCertificate = "c:\\Users\\ADMIN\\Downloads\\S2T\\keys\\speechtotext01-7393cf998d12.json";
    @Value("${pathSoundRecord}")
    public String pathSoundRecord = "C:\\Users\\ADMIN\\Desktop\\";
    @Value("${pathOutputText}")
    public String pathOutputText = "c:\\Users\\ADMIN\\Downloads\\S2T\\transcript.txt";
    @Value("${pathUpload}")
    public String pathUpload;
    @Value("${pathSoundIvr}")
    public String pathSoundIvr;

    static String transId = Common.randomString(Constants.TRANS_ID_LENGTH);


    public static String getRecordingFileName(String channelStr, String callerNumber, String called) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String strDate = dateFormat.format(new Date());
        channelStr = channelStr.replaceAll("/", "");
        return String.format("%s_%s_%s_%s.wav", strDate, channelStr, callerNumber, called);
    }

    public static String generateTransId() {
        String transId = Common.randomString(Constants.TRANS_ID_LENGTH);
        return transId;
    }

    protected String convert(@RequestParam("file") String fileName) {
        try {
            // Reads the audio file into memory
            if (!StringUtils.isEmpty(fileName)) {
                fileName = pathSoundIvr + fileName;
                Path path = Paths.get(fileName);
                byte[] data = Files.readAllBytes(path);
                String encoded = Base64.getEncoder().encodeToString(data);
                System.out.println("[SUCCESS]convert");
                return encoded;// Base64.encodeBase64(data);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
        // return new byte[] {};
    }


    protected byte[] byteconvert(@RequestParam("file") String fileName) {
        try {
            // Reads the audio file into memory
            if (!StringUtils.isEmpty(fileName)) {
                fileName = pathSoundIvr + fileName;
                Path path = Paths.get(fileName);
                byte[] data = Files.readAllBytes(path);
                // String encoded = Base64.getEncoder().encodeToString(data);
                byte[] encoded = Base64.getEncoder().encode(data);
                System.out.println("[SUCCESS]byteconvert");
                return encoded;// Base64.encodeBase64(data);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new byte[]{};
    }
}
