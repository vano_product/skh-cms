package vn.vano.bo;

import vn.vano.dao.IGenericDao;

import java.io.Serializable;
import java.util.List;

public interface IGenericBo<T extends Serializable, PK extends Serializable> {

    public <E extends IGenericDao<T, PK>> E getDAO();

    T findOne(PK id);

    List<T> findAll();

    /**
     * CRUD
     */
    public abstract void create(T record);

    public abstract T read(PK id);

    public abstract void update(T record);

    public abstract void delete(T record);

    void deleteById(PK entityId);

}
