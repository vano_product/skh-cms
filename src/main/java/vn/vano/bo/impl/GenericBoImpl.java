package vn.vano.bo.impl;

import org.springframework.transaction.annotation.Transactional;
import vn.vano.bo.IGenericBo;
import vn.vano.dao.IGenericDao;

import java.io.Serializable;
import java.util.List;

@Transactional
public abstract class GenericBoImpl<T extends Serializable, PK extends Serializable> implements IGenericBo<T, PK> {

    @Override
    public abstract <E extends IGenericDao<T, PK>> E getDAO();

    @Override
    public T findOne(PK id) {
        return this.getDAO().findOne(id);
    }

    @Override
    public List<T> findAll() {
        return this.getDAO().findAll();
    }

    @Override
    public void create(T record) {
        this.getDAO().create(record);
    }

    @Override
    public T read(PK id) {
        return this.getDAO().read(id);
    }

    @Override
    public void update(T record) {
        this.getDAO().update(record);
    }

    @Override
    public void delete(T record) {
        this.getDAO().delete(record);
    }

    @Override
    public void deleteById(PK entityId) {
        this.getDAO().deleteById(entityId);
    }
}
