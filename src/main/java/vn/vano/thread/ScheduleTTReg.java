package vn.vano.thread;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.vano.bean.RequestLogList;
import vn.vano.bean.RequestLogsBean;
import vn.vano.common.Common;
import vn.vano.common.IGsonBase;
import org.springframework.scheduling.annotation.Scheduled;
import vn.vano.jpa.RequestLogsTT;
import vn.vano.service.CommonService;
import vn.yotel.commons.context.AppContext;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScheduleTTReg implements IGsonBase {
    private final Logger LOG = LoggerFactory.getLogger(ScheduleTTReg.class);
    CommonService commonService = (CommonService) AppContext.getBean("commonService");

    @Scheduled(cron = "0 30 01 * * ?")
    public void SyncTTReg(){
        LOG.debug("BEGIN::SyncTTReg");
        DateTime dtCurrent = new DateTime();
        String transId = String.valueOf(System.currentTimeMillis());
        try {
        String strDate = Common.dateToString(dtCurrent.minusDays(1).toDate(), Common.DATE_YYYY_MM_DD);
//        String ulr = "http://vnpt.mytalk.vn/api/list-sub-reg?date=" + strDate;
//            String ulr = "http://skh.cuchot.vn/api/list-sub-reg?date=" + strDate;
            String ulr = "http://ads.cuchot.vn/api/list-sub-reg?date=" + strDate.trim() + "&partner=SKH";
//            String ulr = "http://ads.cuchot.vn/api/list-sub-reg?date=2021-10-29&partner=SKH";
//            String ulr = "http://skh.cuchot.vn/api/list-sub-reg?date="+strDate ;
            HttpResponse<String> response = Unirest.get(ulr)
                    .asString();
            if (response != null ){
                String json = response.getBody().toString();

//               String strjson =  String.valueOf(response.getBody()).replace("[","{").replace("]","}");

//                RequestLogList lstbean=  GSON.fromJson(json, RequestLogList.class);
//                List<RequestLogsBean> list = lstbean.getList();
                ObjectMapper mapper = new ObjectMapper();
                List<RequestLogsBean> list = Arrays.asList(mapper.readValue(json, RequestLogsBean[].class));
//                RequestLogsBean[] pp1 = mapper.readValue(json, RequestLogsBean[].class);
                if(list!= null && list.size()>0){
                    list.stream().forEach(item -> {
//                        RequestLogsBean rqBean = item;
                        RequestLogsTT requestLogsTT = new RequestLogsTT();
                        requestLogsTT.setMsisdn(item.getMsisdn());
                        requestLogsTT.setActionDate(item.getCreated_date());
                        requestLogsTT.setLinkEeq(item.getUrl());
//                        rqBean = Common.convertToBean(item,rqBean);
                        commonService.create(transId,requestLogsTT);
                    });
                }
            }else {
                LOG.error(transId + ";Call API SyncTTReg  error");
            }
            RequestLogsTT requestLogsTT = new RequestLogsTT();
            requestLogsTT.setActionDate(Common.dateToString(dtCurrent.toDate(), Common.DATE_YYYY_MM_DD));
            commonService.create(transId,requestLogsTT);
        } catch (Exception ex) {
            LOG.error(transId, ex);
        }
        LOG.debug(transId+" END::SyncTTReg");
    }

}
