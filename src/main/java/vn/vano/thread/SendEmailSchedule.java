//package vn.vano.thread;
//
//import org.joda.time.DateTime;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.scheduling.annotation.Scheduled;
//import vn.vano.bean.AwardResultCTKMBean;
//import vn.vano.bean.EffectCTKMBean;
//import vn.vano.bean.QuantityDayBean;
//import vn.vano.bean.ReportMailDailyBean;
//import vn.vano.common.*;
//import vn.vano.service.CommonService;
//import vn.yotel.commons.context.AppContext;
//
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.regex.Pattern;
//
//public class SendEmailSchedule implements IGsonBase {
//    private final Logger LOG = LoggerFactory.getLogger(SendEmailSchedule.class);
//    private final SimpleDateFormat sdf_DDMMYYYY = new SimpleDateFormat("dd/MM/yyyy");
//
//    MailSender mailSender = (MailSender) AppContext.getBean("mailSender");
//    CommonService commonService = (CommonService) AppContext.getBean("commonService");
//
//
//    //second, minute, hour, day of month, month, day(s) of week
//    //Send email at 8h ngay thu 5 hang tuan
//    @Scheduled(cron = "0 0 9 * * THU")
//    public void sendEmailWAP(){
//        String transId = Common.randomString(Constants.TRANS_ID_LENGTH);
//        LOG.debug("BEGIN::sendEmailCTKM");
//        HashMap<String, Object> data = new HashMap<>();
//        try {
//
//            DateTime dtCurrent = new DateTime();
//            Date toDate = dtCurrent.toDate();//Ngay thu 5 tuan N
//            Date fromDate = dtCurrent.plusDays(-7).toDate();//Ngay thu 5 tuan N - 1
//            String strTodate = Common.dateToString(toDate, Common.FORM_DATE_DDMMYYYY);
//            String strFromdate = Common.dateToString(fromDate, Common.FORM_DATE_DDMMYYYY);
//            CorePromotion promotion = null;
//            List<Object[]> lstPromoObj = commonService.findBySQL(transId, "SELECT * FROM core_promotion WHERE end_time >= ? ", new String[]{Common.dateToString(toDate, "yyyy-MM-dd")});
//            if(!Common.isEmpty(lstPromoObj)) {
//                promotion = new CorePromotion();
//                promotion = Common.convertToBean(lstPromoObj.get(0), promotion);
//            }
//            if (promotion != null) {
//                Map<String, Object> params = new HashMap<>();
//                List<String> lstParam = new ArrayList<>();
//                String sql = " select   num_subs_register,num_subs_psc,num_subs_topup,num_subs_is_topup ,concat(round(( num_subs_is_topup/num_subs_topup * 100 ),2),'%') percent_topup, (num_subs_register - num_subs_topup) num_subs_not_topup, 0 num_subs_unreg_topup\n" +
//                        " from  ( select   (select count(1) from (select msisdn, min(reg_time) reg_time from ivr_sync_subs_ctkm \n" +
//                        "  where  reg_time >= STR_TO_DATE(?,'%d/%m/%Y') and reg_time < STR_TO_DATE(?,'%d/%m/%Y') \n" +
//                        "  and subs_type in ('REGNEW','EXTEND','BUY')   group by msisdn) a )  num_subs_register,        \n" +
//                        "   (select count(1) from  (select  msisdn, min(reg_time) reg_time \n" +
//                        "               from ivr_sync_subs_ctkm where reg_time >= STR_TO_DATE(?,'%d/%m/%Y') \n" +
//                        "               and reg_time < STR_TO_DATE(?,'%d/%m/%Y') and charge_price > 0   group by msisdn) a) num_subs_psc,      \n" +
//                        "    (select count(1) from (select * from ivr_sync_subs_ctkm where " +
//                        "     reg_time >= STR_TO_DATE(?,'%d/%m/%Y') and reg_time < STR_TO_DATE(?,'%d/%m/%Y')  and action = '1' and topup_amount > 0 ) a ) num_subs_topup,\n" +
//                        "  (select count(1) from (select * from ivr_sync_subs_ctkm where  \n" +
//                        "    reg_time >= STR_TO_DATE(?,'%d/%m/%Y') and reg_time < STR_TO_DATE(?,'%d/%m/%Y')  AND is_topup = '1' ) a ) num_subs_is_topup\n" +
//                        "   from core_promotion limit 1 ) xx";
////                List<Object[]> lstObject = commonService.callProcedure(transId, "sp_topup_sum_report", params);
//                lstParam.add(strFromdate.trim());
//                lstParam.add(strTodate.trim());
//
//                lstParam.add(strFromdate.trim());
//                lstParam.add(strTodate.trim());
//
//                lstParam.add(strFromdate.trim());
//                lstParam.add(strTodate.trim());
//
//                lstParam.add(strFromdate.trim());
//                lstParam.add(strTodate.trim());
//
////                List<Object[]> lstObject = commonService.findBySQL(transId, sql, lstParam.toArray());
//
//                List<AwardResultCTKMBean> lstReportCTKM = new ArrayList<>();
//                List<Object[]> lstObj = commonService.findBySQL(transId, sql, lstParam.toArray());
//                if(lstObj != null && !lstObj.isEmpty()) {
//                    lstObj.stream().forEach(item -> {
//                        AwardResultCTKMBean bean = new AwardResultCTKMBean();
//                        bean = Common.convertToBean(item, bean);
//                        lstReportCTKM.add(bean);
//                    });
//                }
//                data.put("reportCTKM", lstReportCTKM);
//
//
//                String sqlEffect = " select service_name, name  ,note, topup_type,  num_subs_register, num_subs_unreg_topup, (num_subs_register - num_subs_unreg_topup ) num_subs_active,  num_subs_psc,  revenue_cycle_report, revenue_accumulate ,cpkm_amount , 0 differrent " +
//                        "  from \n" +
//                        " (  select 'MyTalk 4.0' service_name, b.name, CONCAT('Từ ',DATE_FORMAT(b.start_time, '%d/%m/%Y'),' - ', DATE_FORMAT(b.end_time, '%d/%m/%Y')) note, 'TKC' topup_type ,\n" +
//                        "  (select count(1) from ( select  msisdn, min(reg_time) reg_time from ivr_sync_subs_ctkm where  reg_time >= STR_TO_DATE(?,'%d/%m/%Y') and reg_time < STR_TO_DATE(?,'%d/%m/%Y') \n" +
//                        "   and subs_type in ('REGNEW','EXTEND','BUY')  group by msisdn ) a) num_subs_register,\t\n" +
//                        "   (select count(1) from ( select  msisdn, max(reg_time) reg_time from ivr_sync_subs_ctkm where   reg_time >= STR_TO_DATE(?,'%d/%m/%Y') and reg_time < STR_TO_DATE(?,'%d/%m/%Y') \n" +
//                        "   and subs_type in ('CANCEL')  group by msisdn ) a) num_subs_unreg_topup,\n" +
//                        "    (select count(1) from  (select  msisdn, min(reg_time) reg_time from ivr_sync_subs_ctkm where  reg_time >= STR_TO_DATE(?,'%d/%m/%Y') and reg_time < STR_TO_DATE(?,'%d/%m/%Y') \n" +
//                        "                and charge_price > 0  group by msisdn) a) num_subs_psc,\n" +
//                        "  (select * from  (SELECT SUM(amount) FROM core_charge_log where amount > 40 and package_code = 'P' and created_time  >= STR_TO_DATE(?,'%d/%m/%Y') and created_time  < STR_TO_DATE(?,'%d/%m/%Y') \n" +
//                        "                   ) a) revenue_cycle_report,\n" +
//                        " (select * from  (SELECT SUM(amount) FROM core_charge_log where amount > 40 and package_code = 'P' and created_time  >= '2019-12-12' and created_time  < STR_TO_DATE(?,'%d/%m/%Y')" +
//                        "                   ) a) revenue_accumulate,\n" +
//                        " (select sum(topup_amount) from  (select topup_amount  from ivr_sync_subs_ctkm where  reg_time >= STR_TO_DATE(?,'%d/%m/%Y') and reg_time < STR_TO_DATE(?,'%d/%m/%Y') \n" +
//                        "   and is_topup = '1' ) a) cpkm_amount, " +
//                        "    0 differrent\n" +
//                        "from core_promotion b where 1 limit  1 ) xx ";
//                List<String> lstParamEffect = new ArrayList<>();
//                lstParamEffect.add(strFromdate.trim());
//                lstParamEffect.add(strTodate.trim());
//
//                lstParamEffect.add(strFromdate.trim());
//                lstParamEffect.add(strTodate.trim());
//
//                lstParamEffect.add(strFromdate.trim());
//                lstParamEffect.add(strTodate.trim());
//
////                lstParam.add(reportKey.trim());
//                lstParamEffect.add(strFromdate.trim());
//                lstParamEffect.add(strTodate.trim());
////                lstParamEffect.add(strFromdate.trim());
//                lstParamEffect.add(strTodate.trim());
//
////                lstParam.add(reportKey.trim());
//                lstParamEffect.add(strFromdate.trim());
//                lstParamEffect.add(strTodate.trim());
//
//                List<EffectCTKMBean> lstReportCTKMEffect = new ArrayList<>();
//
//
//                List<Object[]> lstEffectObject = commonService.findBySQL(transId, sqlEffect, lstParamEffect.toArray());
//
//                if(!Common.isEmpty(lstEffectObject)) {
//                    lstEffectObject.stream().forEach(item -> {
//                        EffectCTKMBean bean = new EffectCTKMBean();
//                        bean = Common.convertToBean(item, bean);
//                        lstReportCTKMEffect.add(bean);
//                    });
//                }
//                data.put("reportCTKMEffect", lstReportCTKMEffect);
//                List<Object[]> lstObjMailCC = commonService.findBySQL("", "SELECT * FROM sys_param WHERE _key = 'LIST_MAIL_CC'", null);
//                List<String> lstMailCC = new ArrayList<>();
//                String[] arrEmailCC = null;
//                if (!Common.isEmpty(lstObjMailCC)) {
//                    Object[] objCC = lstObjMailCC.get(0);
//                    String strEmailCC = String.valueOf(objCC[2]);
//                    arrEmailCC= strEmailCC.split(";");
////                    Arrays.stream(arrEmailCC).forEach(item -> {
////                        lstMailCC.add(item);
////                    });
//                }
//
//                String htmlContent = ThymeleafUtil.getHtmlContentInClassPath("report-ctkm.html", data);
//                mailSender.sendHtmlEmail(transId,"Báo cáo kết quả trao thưởng theo CTKM tuần từ "+ strFromdate
//                        + " đến " + strTodate, htmlContent, arrEmailCC);
//            }
//        } catch (Exception ex) {
//            LOG.debug("", ex);
//        }
//        LOG.debug("END::sendEmailCTKM");
//    }
//
//}
