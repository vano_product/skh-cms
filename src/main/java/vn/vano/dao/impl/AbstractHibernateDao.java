package vn.vano.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

public abstract class AbstractHibernateDao<T extends Serializable, PK extends Serializable> {

    private Class<T> clazz;

    @Resource
    SessionFactory sessionFactory;

    public final void setClazz(Class<T> clazzToSet) {
        this.clazz = clazzToSet;
    }

    @SuppressWarnings("unchecked")
    public T findOne(PK id) {
        return (T) getCurrentSession().get(clazz, id);
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return getCurrentSession().createQuery("from " + clazz.getName()).list();
    }

    public void create(T entity) {
        getCurrentSession().persist(entity);
    }

    @SuppressWarnings("unchecked")
    public T read(PK id) {
        return (T) getCurrentSession().get(clazz, id);
    }

    public T update(T entity) {
        getCurrentSession().merge(entity);
        return entity;
    }

    public void delete(T entity) {
        getCurrentSession().delete(entity);
    }

    public void deleteById(PK entityId) {
        T entity = findOne(entityId);
        delete(entity);
    }

    protected final Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
