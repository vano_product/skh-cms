package vn.vano.dao.impl;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import vn.vano.dao.IGenericDao;

import java.io.Serializable;


@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GenericHibernateDao<T extends Serializable, PK extends Serializable> extends AbstractHibernateDao<T, PK> implements IGenericDao<T, PK> {
}