package vn.vano.common;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.util.List;

public class RestCallApp {
    private final Logger LOG = LoggerFactory.getLogger(RestCallApp.class);
    protected static CloseableHttpClient httpclient;

    static {
        try {
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(SSLContext.getDefault(), NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", new PlainConnectionSocketFactory())
                    .register("https", sslsf)
                    .build();
            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
            cm.setMaxTotal(100);
//            httpclient = HttpClients.createDefault();
            httpclient = HttpClients.custom()
                    .setSSLSocketFactory(sslsf)
                    .setConnectionManager(cm)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public RestMessage sendEmail(String host, String subject, String message, String emailForm, List<String> emailTo){
        RestMessage restMessage = new RestMessage();
        StringBuilder stringBuilder = new StringBuilder();
        for (String item : emailTo) {
            stringBuilder.append(item);
            stringBuilder.append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        String toStr = stringBuilder.toString();
        HttpResponse<JsonNode> resp = null;
        try {
            resp = Unirest.post(host + sendEmailAPI).header("Content-Type", "application/x-www-form-urlencoded")
                    .field("message", message).field("subject", subject).field("from", emailForm).field("to", toStr)
                    .asJson();
            if (resp == null) {
                return RestMessage.RestMessageBuilder.FAIL("0", "FAIL");
            }
            String respStatus = resp.getBody().getObject().get("status").toString();
            String respMessage = resp.getBody().getObject().get("message").toString();
            restMessage.setStatus(respStatus);
            restMessage.setMessage(respMessage);
        } catch (Exception ex){
            LOG.error("", ex);
            return RestMessage.RestMessageBuilder.FAIL("9999", "ERROR");
        }

        return restMessage;
    }

    public RestMessage callLocalApi(String urlWithParam) {
        RestMessage restMessage = new RestMessage();
        HttpResponse<JsonNode> resp = null;
        try {
            if(urlWithParam.matches(Constants.G_REGEX_API)) {
                LOG.info(urlWithParam);
                resp = Unirest.get(urlWithParam).header("Content-Type", "application/x-www-form-urlencoded").asJson();
                if (resp == null) {
                    return RestMessage.RestMessageBuilder.FAIL("0", "FAIL");
                }
                String respStatus = resp.getBody().getObject().get("code").toString();
                String respMessage = resp.getBody().getObject().get("message").toString();
                restMessage.setStatus(respStatus);
                restMessage.setMessage(respMessage);
            } else {
                return RestMessage.RestMessageBuilder.FAIL("0", "URL INVALID");
            }
        } catch (Exception ex) {
            LOG.error("", ex);
            return RestMessage.RestMessageBuilder.FAIL("9999", "ERROR");
        }
        return restMessage;
    }


    public RestMessage unSubsPackage( String  msisdn ,String commandCode, String pkgCode,String channel){
        LOG.info( "::BEGIN::unSubsPackage");
        String result = null;
        RestMessage restMessage = new RestMessage();
        try {
            String xml =
//                    "<?xml version='1.0' encoding='UTF-8'?>" +
                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:obj=\"http://object.app.telsoft/\">" +
                            "<soapenv:Header/>" +
                            "<soapenv:Body>" +
                            "   <obj:receiverServiceReq>" +
                            "       <ISDN>" + msisdn + "</ISDN>" +
                            "       <ServiceCode>" +  "1048" + "</ServiceCode>" +
                            "       <CommandCode>" + commandCode + "</CommandCode>" +
                            "       <PackageCode>" + pkgCode + "</PackageCode>" +
                            "       <SourceCode>" + channel + "</SourceCode>" +
//                    "       <RequestId>" + System.currentTimeMillis() + "</RequestId>" +
                            "       <User>" + Common.upper("MYTALK") + "</User>" +
                            "       <Password>" + "mytalk@132" + "</Password>" +
                            "       <Description>" + "" + "</Description>" +
                            "   </obj:receiverServiceReq>" +
                            "</soapenv:Body>" +
                            "</soapenv:Envelope>";
            HttpPost post = new HttpPost("https://10.3.60.49" + "/mbfn/sb/SOAPRequestServicecps/receiverServiceReq");
            LOG.debug("CALL UNREG::" + ("https://10.3.60.49/mbfn/sb/SOAPRequestServicecps/receiverServiceReq"));
            CloseableHttpResponse resp = null;
            ByteArrayEntity entity = new ByteArrayEntity(xml.getBytes("UTF-8"));
            post.setEntity(entity);
            post.addHeader("Content-Type", "text/xml; charset=utf-8");
            post.addHeader("Connection", "close");
            post.addHeader("x-ibm-client-id", "dcd5cf4f-e572-4a06-a00d-9e6116157c29");
            LOG.debug("::UNREG_REQ::" + Common.unicode2ASII(xml.replace("\n", " ")
                    .replace("\t", " ").replaceAll(" +", " ")));
            //Lay thoi gian bat dau goi API
            long lstart = System.currentTimeMillis();

            resp = httpclient.execute(post);
            //Lay thoi gian ket thuc goi API
            long lend = System.currentTimeMillis();
            LOG.info("::UNREG::TIME=" + (lend - lstart));

            String str = Common.readInputStream(resp.getEntity().getContent()) + "";
//            LOG.info(transId + "::UNREG_RESP_1::" + str);
            LOG.info( "::UNREG_RESP::" + Common.unicode2ASII(StringEscapeUtils.unescapeHtml4(str.replace("\n", " ")
                    .replace("\t", " ").replaceAll(" +", " "))));

            if (resp != null) {
                try {
                    resp.close();
                } catch (Exception e) {
                }
            }
            post.releaseConnection();
            if (str.contains("HUY_P_SUCC") || str.contains("HUY_M_SUCC")) {
                restMessage.setStatus("1");
                restMessage.setMessage("OK");
            }
        } catch (Exception ex) {
            LOG.error("", ex);
            restMessage.setStatus("0");
        } finally {
            LOG.info("::END::unSubsPackage");
        }
        return restMessage;
    }

//    public RestMessage unSubsPackage(String msisdn, String commandCode, String pkgCode, String userName) {
//        LOG.info("BEGIN::unSubsPackage");
//        RestMessage restMessage = new RestMessage();
//        HttpResponse<JsonNode> resp = null;
//        try {
//            LOG.debug("CALL::" + (host + unregSubsAPI));
//            resp = Unirest.post(host + unregSubsAPI)
//                    .header("Content-Type", "application/x-www-form-urlencoded")
//                    .field("command", commandCode)
//                    .field("msisdn", msisdn)
//                    .field("pkg", pkgCode)
//                    .field("username", userName)
//                    .field("channel", "CP")
//                    .asJson();
//            if (resp == null) {
//                return RestMessage.RestMessageBuilder.FAIL("0", "FAIL");
//            }
//            String respStatus = resp.getBody().getObject().get("code").toString();
//            String respMessage = resp.getBody().getObject().get("message").toString();
//            restMessage.setStatus(respStatus);
//            restMessage.setMessage(respMessage);
//        } catch (Exception ex) {
//            LOG.error("", ex);
//            return RestMessage.RestMessageBuilder.FAIL("9999", "ERROR");
//        } finally {
//            LOG.info("END::unSubsPackage");
//        }
//        return restMessage;
//    }

//    public RestMessage getListTopupIbid(String fromDate, String toDate) {
//        RestMessage restMessage = new RestMessage();
//        HttpResponse<String> resp = null;
//        try {
//            resp = Unirest.get(ibidTopupList).header("accept", "application/json")
//                    .queryString("from", fromDate).queryString("to", toDate).asString();
//            if (resp == null) {
//                return RestMessage.RestMessageBuilder.FAIL("0", "FAIL");
//            }
//            String dataResp = resp.getBody();
//            if(dataResp != null && !dataResp.equals("NULL")) {
//                restMessage.setStatus("1");
//                restMessage.setMessage("SUCCESS");
//            } else {
//                restMessage.setStatus("0");
//                restMessage.setMessage("NO_DATA");
//            }
//            restMessage.setData(dataResp);
//        } catch (Exception ex) {
//            LOG.error("", ex);
//            restMessage.setStatus("0");
//            restMessage.setMessage("ERROR");
//        }
//
//        return restMessage;
//    }
//
//    public RestMessage updateStatusTopupIbid(Integer id) {
//        RestMessage restMessage = new RestMessage();
//        HttpResponse<String> resp = null;
//        try {
//            resp = Unirest.get(ibidTopupList).header("accept", "application/json")
//                    .queryString("a", "1").queryString("p", id).asString();
//            if (resp == null) {
//                return RestMessage.RestMessageBuilder.FAIL("0", "FAIL");
//            }
//            String dataResp = resp.getBody();
//            if(dataResp != null && dataResp.equals("SUCCESS")) {
//                restMessage.setStatus("1");
//                restMessage.setMessage("SUCCESS");
//            } else {
//                restMessage.setStatus("0");
//                restMessage.setMessage("FAIL");
//            }
//            restMessage.setData(dataResp);
//        } catch (Exception ex) {
//            LOG.error("", ex);
//            restMessage.setStatus("0");
//            restMessage.setMessage("ERROR");
//        }
//
//        return restMessage;
//    }
//
//    public RestMessage getPromotionIbid() {
//        RestMessage restMessage = new RestMessage();
//        HttpResponse<String> resp = null;
//        try {
//            resp = Unirest.get(ibidTopupList).header("accept", "application/json")
//                    .queryString("a", "2").asString();
//            if (resp == null) {
//                return RestMessage.RestMessageBuilder.FAIL("0", "FAIL");
//            }
//            String dataResp = resp.getBody();
//            if(dataResp != null && !dataResp.equals("ERROR")) {
//                restMessage.setStatus("1");
//                restMessage.setMessage("SUCCESS");
//            } else {
//                restMessage.setStatus("0");
//                restMessage.setMessage("FAIL");
//            }
//            restMessage.setData(dataResp);
//        } catch (Exception ex) {
//            LOG.error("", ex);
//            restMessage.setStatus("0");
//            restMessage.setMessage("ERROR");
//        }
//
//        return restMessage;
//    }

    private String host;
    private String from;
    private String sendEmailAPI = "/custcare/vtvsb/send_email.html";
//    private String ibidTopupList = "http://daugiaibid.vn/ccsp/topup/promo.jsp";
    private String pathTemplate;
    private String pathHtmlData;
    private String pathHtmlResult;
    private String unregSubsAPI = "";

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSendEmailAPI() {
        return sendEmailAPI;
    }

    public void setSendEmailAPI(String sendEmailAPI) {
        this.sendEmailAPI = sendEmailAPI;
    }

    public String getPathTemplate() {
        return pathTemplate;
    }

    public void setPathTemplate(String pathTemplate) {
        this.pathTemplate = pathTemplate;
    }

    public String getPathHtmlData() {
        return pathHtmlData;
    }

    public void setPathHtmlData(String pathHtmlData) {
        this.pathHtmlData = pathHtmlData;
    }

    public String getPathHtmlResult() {
        return pathHtmlResult;
    }

    public void setPathHtmlResult(String pathHtmlResult) {
        this.pathHtmlResult = pathHtmlResult;
    }

    public String getUnregSubsAPI() {
        return unregSubsAPI;
    }

    public void setUnregSubsAPI(String unregSubsAPI) {
        this.unregSubsAPI = unregSubsAPI;
    }
}
