package vn.vano.common;

import java.util.ArrayList;

public interface IVRConstants {

    public int MAX_NUM_OF_DIAL_TRY = 4;

    public String SOUND_PATH = "/ivrcskh_asterisk/sounds/";
    public String RECORDING_PATH = "/ivrcskh_asterisk/recording/";

    public interface AsteriskContext {
        public String DIAL_IN = "dial_in";
        public String GREATING_CONTEXT = "greating_context";
        public String CALL_SUPPORT = "call_support";
    }

    public interface ProductMode {
        public int DEVELOPMENT = 0;
        public int INTERNAL_TEST = 1;
        public int SMALL_TEST = 2;
        public int PRODUCTION = 3;
    }

    public interface AGI_PARAM {
        public String SERVING_TIME = "serving_time_param";
    }

    public interface AGI_VARS {
        public String AGI_CALL_STATUS = "agi_call_status";
        public String SERVING_TIME = "serving_time";
        public String CALL_UUID = "call_uuid";
        public String TARGET_NUMBER = "target_number";
        public String TARGET_NUMBER_CC = "target_number_cc";
        public String RECORDING_FILE_VAR = "recording_file";
        public String DIAL_STATUS = "dial_status";
        public String PRODUCT_ID = "product_id";
    }

    public interface DIAL_EVENT {

        public String BEGIN = "Begin";
        public String END = "End";
    }

    public interface DIAL_STATUS {

        public String CHANUNAVAIL = "CHANUNAVAIL";
        public String CONGESTION = "CONGESTION";
        public String NOANSWER = "NOANSWER";
        public String BUSY = "BUSY";
        public String CANCEL = "CANCEL";
    }

    public interface SUPPORT_CALL_STATUS {

        public String USER_BANNED = "USER_BANNED";
        public String ALL_BUSY = "ALL_BUSY";
        public String MAX_CALL_TRIED = "MAX_CALL_TRIED";
        public String NOT_SERVING = "NOT_SERVING";
        public String FAILED = "FAILED";
        public String RINGING = "RINGING";
        public String CALLING = "CALLING";
        public String ANSWER = "ANSWER";
        public String CANCEL = "CANCEL";
    }

    public static ArrayList<String> DIAL_STATUS_LIST = new ArrayList<String>() {
        private static final long serialVersionUID = -870725595288568943L;

        {
            add(DIAL_STATUS.CHANUNAVAIL);
            add(DIAL_STATUS.CONGESTION);
            add(DIAL_STATUS.NOANSWER);
            add(DIAL_STATUS.BUSY);
            add(DIAL_STATUS.CANCEL);
        }
    };

    public interface ChannelStateDesc {

        public String DOWN = "Down";
        public String RSRVD = "Rsrvd";
        public String OFFHOOK = "OffHook";
        public String DIALING = "Dialing";
        public String RING = "Ring";
        public String RINGING = "Ringing";
        public String UP = "Up";
        public String BUSY = "Busy";
    }

    public interface AccountType {

        public byte ACCOUNT_USAGE_BILLING = 0;
        public byte ACCOUNT_TIMELY_CIRCLE_BILLING = 1;
    }

    public interface AnswerStatus {

        public String WAITING = "WAITING";
        public String RINGING = "RINGING";
        public String TALKING = "TALKING";
        public String BUSY = "BUSY";
    }

    public interface CallAgiStatus {

        public String SUCCESS = "1";
        public String FAILURE = "0";
    }

    public interface CallDirection {

        public byte CALL_IN = 0;
        public byte CALL_OUT = 1;
    }

    public interface CallType {
        public int SUPPORT_CALL = 0;
        public int CORP_CALL = 1;
    }

    public interface ServiceCode {

        public String SC_190001005 = "19000105";
        public String SC_CALLOUT = "0473056115";
    }

    public interface SocialNetworkName {
        public String FB = "Facebook";
    }

    public interface UserType {
        public byte SYSTEM_USER = 1;
        public byte AFFILIATE_USER = 2;
    }

    public interface CallDetailStatus {
        public byte UNKNOWN = 0;
        public byte ANSWERED = 1;
        public byte NOANSWER = 2;
        public byte CANCEL = 3;
        public byte CONGESTION = 4;
        public byte CHANUNAVAIL = 5;
        public byte BUSY = 6;
    }

    public interface ServiceNumbers {
        public String YOTEL = "19000105";
        public String VANO = "2471060808";
    }

}
