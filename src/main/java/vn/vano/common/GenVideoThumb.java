/*
package vn.vano.common;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;

public class GenVideoThumb {
    private static final Logger LOG = LoggerFactory.getLogger(GenVideoThumb.class);

    public static String generate(String msisdn, String pathVideoMp4, String pathImageThumb, String imgType) throws Exception
    {
        LOG.info("BEGIN::generateThumbnail");
//        File fThumbWrite = null;
        Java2DFrameConverter converter = new Java2DFrameConverter();
        FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber(pathVideoMp4);
        String path = "";
        try {
            Frame frame;
            frameGrabber.start();
            if (frameGrabber.getLengthInFrames() < 240) {
                int numSecond = frameGrabber.getLengthInFrames()/24;
                frameGrabber.setFrameNumber(numSecond*24);
                frame = frameGrabber.grab();
            } else {
                frameGrabber.setFrameNumber(240);
                frame = frameGrabber.grab();
            }

            BufferedImage bi = converter.convert(frame);
            path = pathImageThumb + msisdn + "_" + System.currentTimeMillis() + "." + imgType;

            ImageResizer.resize(bi, path, 0.5);
//            fThumbWrite = new File(path);
//            OutputStream outputStream = new FileOutputStream(fThumbWrite, true);
//            ImageIO.write(bi, imgType, outputStream);
//            outputStream.close();

            frameGrabber.stop();
            LOG.info(">>>::Write and resize 50% thumbnail file " + path);
        } catch (Exception ex) {
            LOG.error("", ex);
        } finally {
//            if (fThumbWrite != null) {
//                fThumbWrite.delete();
//                LOG.info(">>>::Delete temp file " + path);
//            }
            LOG.info("END::generateThumbnail");
        }
        return path;
    }

    public static void main(String[] args) {
        try {
            String png = generate("899489669", "C:\\VICALL\\VIDEO_899489669_1559905622476.mp4",
                    "C:\\VICALL\\IMAGE\\", "PNG");
//            System.out.println(png);
            String png1 = generate("985922812", "C:\\VICALL\\VIDEO_985922812_1558778505795.mov",
                    "C:\\VICALL\\IMAGE\\", "PNG");
            String png2 = generate("888888888", "C:\\VICALL\\VIDEO_985922812_1558777883511.mov",
                    "C:\\VICALL\\IMAGE\\", "PNG");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
*/
