package vn.vano.common;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameRecorder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class AudioUtils {
    public static void copyInputStreamToFile(InputStream inputStream, File file)
            throws IOException {
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            int read;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            // commons-io dung LIB
            // IOUtils.copy(inputStream, outputStream);
        } catch (Exception e) {
            System.out.println(e.toString());
        }finally {

        }
    }

    public static void convertWavAudioForAsterisk(File file,File fileOutput) {
        try {
            //E:\SETUP\ffmpeg-20160522-git-566be4f-win64-static\bin>ffmpeg -i path_file\wellcome.mp3 -acodec pcm_s16le -ar 8000 -ac 1 path_output\wellcome.wav
            org.bytedeco.javacv.FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(file.getAbsolutePath());
            grabber.start();
            FrameRecorder recorder = new FFmpegFrameRecorder(fileOutput.getAbsolutePath(), grabber.getAudioChannels());
            recorder.setSampleRate(8000);
            recorder.setAudioChannels(1);
            recorder.setAudioBitrate(128);
            recorder.start();
            Frame frame;
            while ((frame = grabber.grabFrame()) != null) {
                recorder.record(frame);
            }
            recorder.stop();
            grabber.stop();
        } catch (Exception ex) {

        }
    }
}
