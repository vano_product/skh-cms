package vn.vano.common;

import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtils {
    private static Pattern pattern;
    private static Matcher matcher;
    private static final String DATE_PATTERN = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((17|18|19|20|21|22)\\d\\d)";

    public static java.sql.Timestamp createTimestamp() {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        return new java.sql.Timestamp((calendar.getTime()).getTime());
    }

    public static java.sql.Timestamp createDateTimestamp(java.util.Date date) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(date);
        return new java.sql.Timestamp((calendar.getTime()).getTime());
    }

    public static java.sql.Timestamp String2Timestamp(String strInputDate) {
        String strDate = replaceWhiteLetter(strInputDate);
        int i, nYear, nMonth, nDay;
        String strSub = null;
        i = strDate.indexOf("/");
        if (i < 0) {
            return createTimestamp();
        }
        strSub = strDate.substring(0, i);
        nDay = (new Integer(strSub.trim())).intValue();
        strDate = replaceWhiteLetter(strDate.substring(i + 1));
        i = strDate.indexOf("/");
        if (i < 0) {
            return createTimestamp();
        }
        strSub = strDate.substring(0, i);
        nMonth = (new Integer(strSub.trim())).intValue() - 1; // Month begin from 0 value
        strDate = replaceWhiteLetter(strDate.substring(i + 1));
        if (strDate.length() < 4) {
            if (strDate.substring(0, 1).equals("9")) {
                strDate = "19" + strDate.trim();
            } else {
                strDate = "20" + strDate.trim();
            }
        }
        nYear = (new Integer(strDate)).intValue();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(nYear, nMonth, nDay);
        return new java.sql.Timestamp((calendar.getTime()).getTime());
    }

    static boolean isDigitString(String sInput) {
        for (int i = 0; i < sInput.length(); i++) {
            if (!Character.isDigit(sInput.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static java.sql.Timestamp String2TimestampEx(String strInputDate) {
        String strDate = strInputDate;
        if (strDate == null) {
            strDate = "";
        }
        strDate = strDate.replace('o', '0');
        strDate = strDate.replace('O', '0');
        int i, nYear, nMonth, nDay;
        String strSub = null;
        i = getIndexOfSeparate(strDate, 0);
        if (i < 0) {
            return null;
        }
        strSub = strDate.substring(0, i).trim();
        if (!isDigitString(strSub)) {
            return null;
        }
        nDay = new Integer(strSub).intValue();
        strDate = replaceWhiteLetter(strDate.substring(i + 1));
        i = getIndexOfSeparate(strDate, 0);
        if (i < 0) {
            return null;
        }
        strSub = strDate.substring(0, i).trim();
        if (!isDigitString(strSub)) {
            return null;
        }
        nMonth = (new Integer(strSub)).intValue() - 1; // Month begin from 0 value
        strDate = replaceWhiteLetter(strDate.substring(i + 1).trim());
        if (strDate.length() < 4) {
            if (strDate.substring(0, 1).equals("9")) {
                strDate = "19" + strDate.trim();
            } else {
                strDate = "20" + strDate.trim();
            }
        }
        if (!isDigitString(strDate)) {
            return null;
        }
        if (strDate.length() > 6) {
            return null;
        }
        nYear = (new Integer(strDate)).intValue();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(nYear, nMonth, nDay);
        return new java.sql.Timestamp((calendar.getTime()).getTime());
    }

    public static java.sql.Timestamp String2TimestampNew(String strInputDate) {
        String strDate = strInputDate;
        try {
            strDate = strDate.replace('o', '0');
            strDate = strDate.replace('O', '0');
            int i, nYear = 0, nMonth = 0, nDay = 0;
            String strSub = null;
            i = getIndexOfSeparateEx(strDate, 0);
            if (i < 0) {
                strSub = strDate;
                strDate = Timestamp2DDMMYYYY(new Timestamp(System.currentTimeMillis())).
                        substring(3);
            } else {
                strSub = strDate.substring(0, i).trim();
            }
            if (!isDigitString(strSub)) {
                return null;
            }
            try {
                nDay = new Integer(strSub).intValue();
            } catch (NumberFormatException ex) {
                return null;
            }
            strDate = replaceWhiteLetter(strDate.substring(i + 1));
            i = getIndexOfSeparateEx(strDate, 0);
            if (i < 0) {
                strSub = strDate;
                strDate = Timestamp2DDMMYYYY(new Timestamp(System.currentTimeMillis())).
                        substring(6);
            } else {
                strSub = strDate.substring(0, i).trim();
            }
            if (!isDigitString(strSub)) {
                return null;
            }
            try {
                nMonth = (new Integer(strSub)).intValue() - 1; // Month begin from 0 value
            } catch (NumberFormatException ex) {
                return null;
            }
            strDate = replaceWhiteLetter(strDate.substring(i + 1).trim());
            i = getIndexOfSeparateEx(strDate, 0);
            if (i > 0) {
                strDate = replaceWhiteLetter(strDate.substring(0, i));
            }
            if (strDate.length() < 4) {
                if (strDate.length() > 1) {
                    strDate = "20"
                            + strDate.substring(strDate.length() - 2).trim();
                } else if (strDate.length() > 0) {
                    strDate = "200" + strDate.trim();
                }
            }
            if (!isDigitString(strDate)) {
                return null;
            }
            if (strDate.length() > 6) {
                return null;
            }
            try {
                nYear = (new Integer(strDate)).intValue();
            } catch (NumberFormatException ex) {
                return null;
            }
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.set(nYear, nMonth, nDay);
            return new java.sql.Timestamp((calendar.getTime()).getTime());
        } catch (Exception ex) {
            System.out.println("String2TimestampNew loi tai day: " + ex);
            return null;
        }
    }

    public static String getDateTimeString(java.sql.Timestamp ts) {
        if (ts == null) {
            return "";
        }
        return Timestamp2DDMMYYYY(ts) + " " + Timestamp2HHMMSS(ts, 1);
    }

    /*return date with format: dd/mm/yyyy */
    public static String getDateString(java.sql.Timestamp ts) {
        if (ts == null) {
            return "";
        }
        return Timestamp2DDMMYYYY(ts);
    }

    public static String getTimeString(java.sql.Timestamp ts) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        return calendar.get(calendar.HOUR_OF_DAY) + ":"
                + calendar.get(calendar.MINUTE) + ":" + calendar.get(calendar.SECOND);
    }

    public static Timestamp getTimestamp(String dateStr) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
            Timestamp date = new Timestamp(dateFormat.parse(dateStr).getTime());
            return date;
        } catch (Exception ex) {
            System.out.println("Error: " + ex.toString());
            return null;
        }
    }

    public static Timestamp getTimestamp(String dateStr, String format) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            Timestamp date = new Timestamp(dateFormat.parse(dateStr).getTime());
            return date;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String getYYYYMMDDBlank(Timestamp ts) {
        if (ts == null) {
            ts = new Timestamp(System.currentTimeMillis());
        }
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMdd");
        java.util.Date currentTime_1 = new java.util.Date(ts.getTime());
        String monthYear = formatter.format(currentTime_1);
        return monthYear;
    }

    public static String getHHMMSSBlank(Timestamp ts) {
        if (ts == null) {
            ts = new Timestamp(System.currentTimeMillis());
        }
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("HHmmss");
        java.util.Date currentTime_1 = new java.util.Date(ts.getTime());
        String monthYear = formatter.format(currentTime_1);
        return monthYear;
    }

    /*return date with format: dd/mm/yyyy */
    public static String Timestamp2DDMMYYYY(java.sql.Timestamp ts) {
        if (ts == null) {
            return "";
        } else {
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.setTime(new java.util.Date(ts.getTime()));

            String strTemp = Integer.toString(calendar.get(calendar.DAY_OF_MONTH));
            if (calendar.get(calendar.DAY_OF_MONTH) < 10) {
                strTemp = "0" + strTemp;
            }
            if (calendar.get(calendar.MONTH) + 1 < 10) {
                return strTemp + "/0" + (calendar.get(calendar.MONTH) + 1)
                        + "/" + calendar.get(calendar.YEAR);
            } else {
                return strTemp + "/" + (calendar.get(calendar.MONTH) + 1) + "/"
                        + calendar.get(calendar.YEAR);
            }
        }
    }

    /*return date with format: dd/mm/yy */
    public static String Timestamp2DDMMYY(java.sql.Timestamp ts) {
        int endYear;
        if (ts == null) {
            return "";
        } else {
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.setTime(new java.util.Date(ts.getTime()));

            String strTemp = Integer.toString(calendar.get(calendar.DAY_OF_MONTH));
            endYear = calendar.get(calendar.YEAR) % 100;
            if (calendar.get(calendar.DAY_OF_MONTH) < 10) {
                strTemp = "0" + strTemp;
            }
            if (calendar.get(calendar.MONTH) + 1 < 10) {
                if (endYear < 10) {
                    return strTemp + "/0" + (calendar.get(calendar.MONTH) + 1)
                            + "/0" + endYear;
                } else {
                    return strTemp + "/0" + (calendar.get(calendar.MONTH) + 1)
                            + "/" + endYear;
                }
            } else {
                if (endYear < 10) {
                    return strTemp + "/" + (calendar.get(calendar.MONTH) + 1)
                            + "/0" + endYear;
                } else {
                    return strTemp + "/" + (calendar.get(calendar.MONTH) + 1)
                            + "/" + endYear;
                }
            }
        }
    }

    public static String Timestamp2DDMMYYBlank(java.sql.Timestamp ts) {
        int endYear;
        if (ts == null) {
            return "";
        } else {
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.setTime(new java.util.Date(ts.getTime()));

            String strTemp = Integer.toString(calendar.get(calendar.DAY_OF_MONTH));
            endYear = calendar.get(calendar.YEAR) % 100;
            if (calendar.get(calendar.DAY_OF_MONTH) < 10) {
                strTemp = "0" + strTemp;
            }
            if (calendar.get(calendar.MONTH) + 1 < 10) {
                if (endYear < 10) {
                    return strTemp + "0" + (calendar.get(calendar.MONTH) + 1) + "0" + endYear;
                } else {
                    return strTemp + "0" + (calendar.get(calendar.MONTH) + 1) + "" + endYear;
                }
            } else {
                if (endYear < 10) {
                    return strTemp + "" + (calendar.get(calendar.MONTH) + 1) + "0" + endYear;
                } else {
                    return strTemp + "" + (calendar.get(calendar.MONTH) + 1) + "" + endYear;
                }
            }
        }
    }

    public static String Timestamp2YYYYMMDD(java.sql.Timestamp ts) {
        if (ts == null) {
            return "";
        }
        String sYear = "";
        String sMonth = "";
        String sDay = "";
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        // DD
        sDay = "" + Integer.toString(calendar.get(calendar.DAY_OF_MONTH));
        if (calendar.get(calendar.DAY_OF_MONTH) < 10) {
            sDay = "0" + sDay;
        }
        // MM
        if (calendar.get(calendar.MONTH) + 1 < 10) {
            sMonth = "0" + (calendar.get(calendar.MONTH) + 1);
        } else {
            sMonth = "" + (calendar.get(calendar.MONTH) + 1);
        }
        // YYYY
        sYear = "" + calendar.get(calendar.YEAR);

        return sYear + "/" + sMonth + "/" + sDay;
    }

    /**
     * Author: toantt
     *
     * @param ts     Timestapm to convert
     * @param iStyle 0: 24h,  otherwise  12h clock
     * @return
     */
    public static String Timestamp2HHMMSS(java.sql.Timestamp ts, int iStyle) {
        if (ts == null) {
            return "";
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));

        String strTemp;
        if (iStyle == 0) {
            strTemp = Integer.toString(calendar.get(calendar.HOUR_OF_DAY));
        } else {
            strTemp = Integer.toString(calendar.get(calendar.HOUR));
        }

        if (strTemp.length() < 2) {
            strTemp = "0" + strTemp;
        }
        if (calendar.get(calendar.MINUTE) < 10) {
            strTemp += ":0" + calendar.get(calendar.MINUTE);
        } else {
            strTemp += ":" + calendar.get(calendar.MINUTE);
        }
        if (calendar.get(calendar.SECOND) < 10) {
            strTemp += ":0" + calendar.get(calendar.SECOND);
        } else {
            strTemp += ":" + calendar.get(calendar.SECOND);
        }

        if (iStyle != 0) {
            if (calendar.get(calendar.AM_PM) == calendar.AM) {
                strTemp += " AM";
            } else {
                strTemp += " PM";
            }
        }
        return strTemp;
    }

    /**
     * return date time used for 24 hour clock
     */
    public static String getDateTime24hString(java.sql.Timestamp ts) {
        if (ts == null) {
            return "";
        }
        return Timestamp2DDMMYYYY(ts) + " " + Timestamp2HHMMSS(ts, 0);
    }

    /**
     * return date time used for 12 hour clock
     */
    public static String getDateTime12hString(java.sql.Timestamp ts) {
        if (ts == null) {
            return "";
        }
        return Timestamp2DDMMYYYY(ts) + " " + Timestamp2HHMMSS(ts, 1);
    }

    /**
     * return string dd/mm/yyyy from a Timestamp + a addtional day
     *
     * @param ts
     * @param iDayPlus number of day to add
     * @return
     */
    public static String TimestampPlusDay2DDMMYYYY(java.sql.Timestamp ts,
                                                   int iDayPlus) {
        if (ts == null) {
            return "";
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(calendar.DAY_OF_MONTH);
        calendar.set(calendar.DAY_OF_MONTH, iDay + iDayPlus);

        java.sql.Timestamp tsNew = new java.sql.Timestamp((calendar.getTime()).getTime());
        return Timestamp2DDMMYYYY(tsNew);
    }

    public static Timestamp getPreviousDate(Timestamp ts) {
        if (ts == null) {
            return null;
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(calendar.DAY_OF_MONTH);
        calendar.set(calendar.DAY_OF_MONTH, iDay - 1);

        java.sql.Timestamp tsNew = new java.sql.Timestamp((calendar.getTime()).getTime());
        return tsNew;
    }

    /**
     * return the dd/mm/yyyy of current month
     * eg:   05/2002  -->   31/05/2002
     *
     * @param strMonthYear : input string mm/yyyy
     * @return
     */
    public static String getLastestDateOfMonth(String strMonthYear) {
        String strDate = strMonthYear;
        int i, nYear, nMonth, nDay;
        String strSub = null;

        i = strDate.indexOf("/");
        if (i < 0) {
            return "";
        }
        strSub = strDate.substring(0, i);
        nMonth = (new Integer(strSub)).intValue(); // Month begin from 0 value
        strDate = strDate.substring(i + 1);
        nYear = (new Integer(strDate)).intValue();

        boolean leapyear = false;
        if (nYear % 100 == 0) {
            if (nYear % 400 == 0) {
                leapyear = true;
            }
        } else if ((nYear % 4) == 0) {
            leapyear = true;
        }

        if (nMonth == 2) {
            if (leapyear) {
                return "29/" + strDate;
            } else {
                return "28/" + strDate;
            }
        } else {
            if ((nMonth == 1) || (nMonth == 3) || (nMonth == 5)
                    || (nMonth == 7) || (nMonth == 8) || (nMonth == 10)
                    || (nMonth == 12)) {
                return "31/" + strDate;
            } else if ((nMonth == 4) || (nMonth == 6) || (nMonth == 9)
                    || (nMonth == 11)) {
                return "30/" + strDate;
            }
        }
        return "";
    }

    public static int getDayOfWeek(Timestamp ts) {
        if (ts == null) {
            return -1;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(calendar.DAY_OF_WEEK);
        return iDay;
    }

    public static int getDay(Timestamp ts) {
        if (ts == null) {
            return -1;
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(Calendar.DAY_OF_MONTH);
        return iDay;
    }

    public static int getSysDayOfWeek() {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(Calendar.DAY_OF_WEEK);
        return iDay;
    }

    public static int getMonth(Timestamp ts) {
        if (ts == null) {
            return -1;
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iMonth = calendar.get(Calendar.MONTH);
        return iMonth + 1;
    }

    public static int getYear(Timestamp ts) {
        if (ts == null) {
            return -1;
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        return calendar.get(Calendar.YEAR);
    }

    /**
     * Get Date of DAY_OF_WEEK
     *
     * @param DAY_OF_WEEK MONDAY 2
     *                    TUESDAY 3
     *                    WEDNESDAY 4
     *                    THURSDAY 5
     *                    FRIDAY 6
     *                    SATURDAY 7
     *                    SUNDAY 8
     * @param ts          Date input
     * @return Timestam of DAY_OF_WEEK and ts input
     */
    public static Timestamp getDayOfWeekOfDate(int DAY_OF_WEEK, Timestamp ts) {
        if (ts == null) {
            return null;
        }
        if (DAY_OF_WEEK < 2 || DAY_OF_WEEK > 8) {
            return ts;
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        int iDoW = getDayOfWeek(ts);
        int k = 0;
        if (iDoW == Calendar.SUNDAY) {
            iDoW = 8;
        }
        switch (DAY_OF_WEEK) {
            case 2:
                k = Calendar.MONDAY - iDoW;
                break;
            case 3:
                k = Calendar.TUESDAY - iDoW;
                break;
            case 4:
                k = Calendar.WEDNESDAY - iDoW;
                break;
            case 5:
                k = Calendar.THURSDAY - iDoW;
                break;
            case 6:
                k = Calendar.FRIDAY - iDoW;
                break;
            case 7:
                k = Calendar.SATURDAY - iDoW;
                break;
            case 8:
                k = Calendar.SUNDAY - iDoW + 7;
                break;
        }
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, iDay + k);
        java.sql.Timestamp tsNew = new java.sql.Timestamp((calendar.getTime()).getTime());
        return tsNew;
    }

    /**
     * @param DAY_OF_WEEK
     * @param weekOfYear
     * @param ts
     * @return next timestamp defined by day_of_week and weekOfYear
     */
    public static Timestamp getDayOfWeek(int DAY_OF_WEEK, int weekOfYear, Timestamp ts) {
        if (ts == null) {
            return null;
        }
        if (DAY_OF_WEEK < 2 || DAY_OF_WEEK > 8) {
            return ts;
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        int iDoW = getDayOfWeek(ts);
        int weekByTs = getWeekIndexOfYear(ts);
        Timestamp nextTimes = ts;
        if (weekOfYear > weekByTs) {
            int dayNext = (weekOfYear - weekByTs) * 7;
            String sNext = TimestampPlusDay2DDMMYYYY(ts, dayNext);
            nextTimes = getTimestamp(sNext + " 00:00:00");
        }
        int k = 0;
//if day is SUNDAY--> iDoW= 1--> next SATURDAY(7) iDoW= 8;
        if (iDoW == Calendar.SUNDAY) {
            iDoW = 8;
        }
        switch (DAY_OF_WEEK) {
            case 2:
                k = Calendar.MONDAY - iDoW;
                break;
            case 3:
                k = Calendar.TUESDAY - iDoW;
                break;
            case 4:
                k = Calendar.WEDNESDAY - iDoW;
                break;
            case 5:
                k = Calendar.THURSDAY - iDoW;
                break;
            case 6:
                k = Calendar.FRIDAY - iDoW;
                break;
            case 7:
                k = Calendar.SATURDAY - iDoW;
                break;
            case 8:
                k = Calendar.SUNDAY - iDoW + 7;
                break;
        }
        calendar.setTime(new java.util.Date(nextTimes.getTime()));
        int iDay = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, iDay + k);
        java.sql.Timestamp tsNew = new java.sql.Timestamp((calendar.getTime()).getTime());
        return tsNew;
    }

    public static Timestamp getFriday(Timestamp ts) {
        if (ts == null) {
            return null;
        }

        java.util.Calendar calendar = java.util.Calendar.getInstance();
        int iDoW = getDayOfWeek(ts);
        if (iDoW == Calendar.SUNDAY) {
            iDoW = 8;
        }
        int k = Calendar.FRIDAY - iDoW;
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, iDay + k);
        java.sql.Timestamp tsNew = new java.sql.Timestamp((calendar.getTime()).getTime());
        return tsNew;
    }

    public static Timestamp getMonday(Timestamp ts) {
        if (ts == null) {
            return null;
        }

        java.util.Calendar calendar = java.util.Calendar.getInstance();
        int iDoW = getDayOfWeek(ts);
        if (iDoW == Calendar.SUNDAY) {
            iDoW = 8;
        }
        int k = Calendar.MONDAY - iDoW;
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, iDay + k);
        java.sql.Timestamp tsNew = new java.sql.Timestamp((calendar.getTime()).getTime());
        return tsNew;
    }

    public static Timestamp getSunday(Timestamp ts) {
        if (ts == null) {
            return null;
        }

        java.util.Calendar calendar = java.util.Calendar.getInstance();
        int iDoW = getDayOfWeek(ts);
        if (iDoW == Calendar.SUNDAY) {
            iDoW = 8;
        }
        int k = Calendar.SUNDAY - iDoW;
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, iDay + k);
        java.sql.Timestamp tsNew = new java.sql.Timestamp((calendar.getTime()).getTime());
        return tsNew;
    }

    public static boolean isFriday(Timestamp ts) {
        if (ts == null) {
            return false;
        }
        if (getDayOfWeek(ts) == Calendar.FRIDAY) {
            return true;
        }
        return false;
    }

    public static boolean isSaturday(Timestamp ts) {
        if (ts == null) {
            return false;
        }
        if (getDayOfWeek(ts) == Calendar.SATURDAY) {
            return true;
        }
        return false;
    }

    public static boolean isSunday(Timestamp ts) {
        if (ts == null) {
            return false;
        }
        if (getDayOfWeek(ts) == Calendar.SUNDAY) {
            return true;
        }
        return false;
    }

    public static boolean isSysDayOfWeek(int DAY_OF_WEEK) {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        if (getDayOfWeek(ts) == DAY_OF_WEEK) {
            return true;
        }
        return false;
    }

    public static boolean isSysDate(Timestamp ts) {
        if (ts == null) {
            return false;
        }
        String sysDate = Timestamp2DDMMYYYY(createTimestamp());
        String date = Timestamp2DDMMYYYY(ts);
        if (String2Timestamp(date).getTime()
                == String2Timestamp(sysDate).getTime()) {
            return true;
        }

        return false;
    }

    public static boolean isCurrentDay(Timestamp ts) {
        if (ts == null) {
            return false;
        }
        int day = getSystemDay();
        int month = getSystemMonth();
        int year = getSystemYear();
        if (getDay(ts) == day && getMonth(ts) == month
                && getYear(ts) == year) {
            return true;
        }
        return false;
    }

    /**
     * @param DAY_OF_WEEK SUNDAY 1
     *                    MONDAY 2
     *                    TUESDAY 3
     *                    WEDNESDAY 4
     *                    THURSDAY 5
     *                    FRIDAY 6
     *                    SATURDAY 7
     * @param ts          Date input
     * @return true if Date input is DAY_OF_WEEK
     * false other
     */
    public static boolean isDayOfWeek(int DAY_OF_WEEK, Timestamp ts) {
        if (ts == null) {
            return false;
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        if (getDayOfWeek(ts) == DAY_OF_WEEK) {
            return true;
        }
        return false;
    }

    public static String replaceWhiteLetter(String sInput) {
        String strTmp = sInput;
        String sReturn = "";
        boolean flag = true;
        int i = 0;
        while (i < sInput.length() && flag) {
            char ch = sInput.charAt(i);
            if ((ch >= '0' && ch <= '9')
                    || (ch >= 'A' && ch <= 'Z')
                    || (ch >= 'a' && ch <= 'z')) {
                flag = false;
            } else {
                strTmp = sInput.substring(i + 1);
            }
            i++;

        }
        i = strTmp.length() - 1;
        flag = true;
        sReturn = strTmp;
        while (i >= 0 && flag) {
            char ch = strTmp.charAt(i);
            if ((ch >= '0' && ch <= '9')
                    || (ch >= 'A' && ch <= 'Z')
                    || (ch >= 'a' && ch <= 'z')) {
                flag = false;
            } else {
                sReturn = strTmp.substring(0, i);
            }
            i--;
        }
        return sReturn;
    }

    /**
     * @param strInput String input
     * @param type     Type of index 0 at first; 1 reverse
     * @return
     */
    static int getIndexOfSeparate(String strInput, int type) {
        if (type == 1) {
            for (int i = strInput.length() - 1; i >= 0; i--) {
                char ch = strInput.charAt(i);
                if ((ch < '/')
                        || (ch > '9' && ch < 'A')
                        || (ch > 'Z' && ch < 'a')
                        || (ch > 'z')) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < strInput.length(); i++) {
                char ch = strInput.charAt(i);
                if ((ch < '/')
                        || (ch > '9' && ch < 'A')
                        || (ch > 'Z' && ch < 'a')
                        || (ch > 'z')) {
                    return i;
                }
            }
        }
        return -1;
    }

    static int getIndexOfSeparateEx(String strInput, int type) {
        if (type == 1) {
            for (int i = strInput.length() - 1; i >= 0; i--) {
                char ch = strInput.charAt(i);
                if ((ch <= '/')
                        || (ch > '9' && ch < 'A')
                        || (ch > 'Z' && ch < 'a')
                        || (ch > 'z')) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < strInput.length(); i++) {
                char ch = strInput.charAt(i);
                if ((ch <= '/')
                        || (ch > '9' && ch < 'A')
                        || (ch > 'Z' && ch < 'a')
                        || (ch > 'z')) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static long compareInSecond(Timestamp date1, Timestamp date2) {
        long diff = (date1.getTime() - date2.getTime()) / 1000;
        return diff;
    }

    public static long compareSysdateInSecond(Timestamp date) {
        Timestamp date2 = createTimestamp();
        return (date.getTime() - date2.getTime()) / 1000;
    }

    public static long compareSysdateInMiliSecond(Timestamp date) {
        Timestamp date2 = createTimestamp();
        return (date.getTime() - date2.getTime());
    }

    public static long compareSysdateInMiliSecond(String dateTime) {
        Timestamp date = getTimestamp(dateTime);
        Timestamp date2 = createTimestamp();
        return (date.getTime() - date2.getTime());
    }

    public static long compareInMinute(Timestamp date1, Timestamp date2) {
        return (date1.getTime() - date2.getTime()) / 1000 / 60;
    }

    public static long compareSysdateInMinute(Timestamp date) {
        Timestamp date2 = createTimestamp();
        return (date.getTime() - date2.getTime()) / 1000 / 60;
    }

    public static long compareSysDateInHour(Timestamp date) {
        Timestamp date2 = createTimestamp();
        return (date.getTime() - date2.getTime()) / 1000 / 60 / 60;
    }

    public static int getSystemDay() {
        Timestamp date = createTimestamp();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        return (calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static int getSystemMonth() {
        Timestamp date = createTimestamp();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        return (calendar.get(Calendar.MONTH) + 1);
    }

    public static int getSystemYear() {
        Timestamp date = createTimestamp();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        return (calendar.get(Calendar.YEAR));
    }

    public static int getSystemHour() {
        Timestamp date = createTimestamp();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        return (calendar.get(Calendar.HOUR_OF_DAY));
    }

    public static int getHour24(Timestamp date) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        return (calendar.get(Calendar.HOUR_OF_DAY));
    }

    public static int getHour12(Timestamp date) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        return (calendar.get(Calendar.HOUR));
    }

    public static int getSystemMinute() {
        Timestamp date = createTimestamp();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        return (calendar.get(Calendar.MINUTE));
    }

    public static int getMinute(Timestamp date) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        return (calendar.get(Calendar.MINUTE));
    }

    public static long compareToSysDateInDay(Timestamp date) {
        Timestamp date2 = createTimestamp();
        return Math.round((date.getTime() - date2.getTime()) / 1000 / 60 / 60 / 24);
    }

    public static long compareDateAndDateInDay(Timestamp date1, Timestamp date2) {
        return Math.round((date2.getTime() - date1.getTime()) / 1000 / 60 / 60 / 24);
    }

    public static int getWeekIndexOfYear(Timestamp date) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        int woy = calendar.get(Calendar.WEEK_OF_YEAR);
        return woy;
    }

    public static int getCurrentWeekIndexOfYear() {
        Timestamp date = createTimestamp();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(date.getTime()));
        int woy = calendar.get(Calendar.WEEK_OF_YEAR);
        return woy;
    }

    public static int getWeeksFromDate(String sDate) {
        Timestamp date = getTimestamp(sDate);
        Timestamp ts = createTimestamp();
        long time = Math.abs(ts.getTime() - date.getTime());
        return (int) Math.floor(time / 1000 / 60 / 60 / 24 / 7 + 1);
    }

    public static int getWeeksFromDate(Timestamp date) {
        Timestamp ts = createTimestamp();
        long time = ts.getTime() - date.getTime();
        return (int) Math.floor(time / 1000 / 60 / 60 / 24 / 7 + 1);
    }

    public static int getWeeksBetweenDates(Timestamp date1, Timestamp date2) {
        long time = Math.abs(date1.getTime() - date2.getTime());
        return (int) Math.floor(time / 1000 / 60 / 60 / 24 / 7 + 1);
    }

    public static String createFileName(String s) {
        String name = getDateTime24hString(createTimestamp()).
                replaceAll("/", "-");
        name = name.replaceAll(":", "");
        name = name.replaceAll(" ", "");
        return s + name;
    }

    //To process Lottery
// get Date - Month - Year from System
    public static String getDay() {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
                "dd");
        java.util.Date currentTime_1 = new java.util.Date();
        String date = formatter.format(currentTime_1);
        return date;
    }

    public static String getMonth() {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
                "MM");
        java.util.Date currentTime_1 = new java.util.Date();
        String month = formatter.format(currentTime_1);
        return month;
    }

    public static String getYYYY() {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
                "yyyy");
        java.util.Date currentTime_1 = new java.util.Date();
        String year = formatter.format(currentTime_1);
        return year;
    }

    public static String getYY() {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
                "yy");
        java.util.Date currentTime_1 = new java.util.Date();
        String year = formatter.format(currentTime_1);
        return year;
    }

    public static String getMonthYear() {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
                "MM/yyyy");
        java.util.Date currentTime_1 = new java.util.Date();
        String monthYear = formatter.format(currentTime_1);
        return monthYear;
    }

    public static String getDDMM(Timestamp ts) {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd/MM");
        java.util.Date currentTime_1 = new java.util.Date(ts.getTime());
        String ddmm = formatter.format(currentTime_1);
        return ddmm;
    }

    public static String getYYMMDD(Timestamp ts) {
        if (ts == null) {
            ts = new Timestamp(System.currentTimeMillis());
        }
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyMMdd");
        java.util.Date currentTime_1 = new java.util.Date(ts.getTime());
        String monthYear = formatter.format(currentTime_1);
        return monthYear;
    }

    public static String getYYMMDDHHMISS(Timestamp ts) {
        if (ts == null) {
            ts = new Timestamp(System.currentTimeMillis());
        }
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyMMddhhmmss");
        java.util.Date currentTime_1 = new java.util.Date(ts.getTime());
        String monthYear = formatter.format(currentTime_1);
        return monthYear;
    }

    public static String getYYYYMMDDHHMISS(Timestamp ts) {
        if (ts == null) {
            ts = new Timestamp(System.currentTimeMillis());
        }
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddhhmmss");
        java.util.Date currentTime_1 = new java.util.Date(ts.getTime());
        String monthYear = formatter.format(currentTime_1);
        return monthYear;
    }

    public static String getMMDD() {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
                "MMdd");
        java.util.Date currentTime_1 = new java.util.Date();
        String monthYear = formatter.format(currentTime_1);
        return monthYear;
    }

    public static String getEndDateOfMonth(String strMonthYear) {
        String strDate = strMonthYear;
        int i, nYear, nMonth, nDay;
        String strSub = null;

        i = strDate.indexOf("/");
        if (i < 0) {
            return "";
        }
        strSub = strDate.substring(0, i);
        nMonth = (new Integer(strSub)).intValue(); // Month begin from 0 value
        strDate = strDate.substring(i + 1);
        nYear = (new Integer(strDate)).intValue();

        boolean leapyear = false;
        if (nYear % 100 == 0) {
            if (nYear % 400 == 0) {
                leapyear = true;
            }
        } else if ((nYear % 4) == 0) {
            leapyear = true;
        }

        if (nMonth == 2) {
            if (leapyear) {
                return "29";
            } else {
                return "28";
            }
        } else {
            if ((nMonth == 1) || (nMonth == 3) || (nMonth == 5)
                    || (nMonth == 7) || (nMonth == 8) || (nMonth == 10)
                    || (nMonth == 12)) {
                return "31";
            } else if ((nMonth == 4) || (nMonth == 6) || (nMonth == 9)
                    || (nMonth == 11)) {
                return "30";
            }
        }
        return "";
    }

    public static String convertToDateCalendar(String dateInput) {
        int count = 0;
        String ddmmyyyy = "";
        String monthYear = "";
        count = dateInput.indexOf("/");
        if (count < 0) {
            return null;
        }
        String day = dateInput.substring(0, count);
        int intDay = Integer.parseInt(day);
        monthYear = dateInput.substring(count + 1);
        String dateSys = getEndDateOfMonth(monthYear);
        int intDateSys = Integer.parseInt(dateSys);
        if (intDay > intDateSys) {
            int counter = monthYear.indexOf("/");
            if (counter < 0) {
                return null;
            }
            String month = monthYear.substring(0, counter);
            String year = monthYear.substring(counter + 1);
            int intDay1 = intDay - intDateSys;
            int intMonth = Integer.parseInt(month);
            int month1 = intMonth + 1;
            String sDate = String.valueOf(intDay1);
            String sMonth = String.valueOf(month1);
            if (intDay1 < 10) {
                sDate = "0" + sDate;
            }
            if (month1 < 10) {
                sMonth = "0" + sMonth;
            }
            ddmmyyyy = sDate + "/" + sMonth + "/" + year;
        } else {
            int counter = monthYear.indexOf("/");
            if (counter < 0) {
                return null;
            }
            String month = monthYear.substring(0, counter);
            String year = monthYear.substring(counter + 1);
            if (day.length() == 1) {
                day = "0" + day;
            }
            if (month.length() == 1) {
                month = "0" + month;
            }
            if (year.length() < 4) {
                if (year.length() == 1) {
                    year = "200" + year;
                } else if (year.length() == 2) {
                    year = "20" + year;
                } else if (year.length() == 3) {
                    year = "2" + year;
                }
            }
            ddmmyyyy = day + "/" + month + "/" + year;
        }
        return ddmmyyyy;
    }

    public static Timestamp getNextDateN(Timestamp ts, int n) {
        if (ts == null) {
            return null;
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        calendar.add(Calendar.DAY_OF_MONTH, n);
        java.sql.Timestamp tsNew = new java.sql.Timestamp((calendar.getTime()).getTime());
        return tsNew;
    }

    public static String TimestampMultiplyDay2DDMMYYYY(java.sql.Timestamp ts,
                                                       int iDayPlus) {
        if (ts == null) {
            return "";
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        int iDay = calendar.get(Calendar.DAY_OF_MONTH);

        calendar.set(Calendar.DAY_OF_MONTH, iDay * iDayPlus);
        java.sql.Timestamp tsNew = new java.sql.Timestamp((calendar.getTime()).getTime());
        return Timestamp2DDMMYYYY(tsNew);
    }

    //end process date to process XS
//============Calendar=============
    public static double LOCAL_TIMEZONE = 7.0;
    public static final double PI = Math.PI;
    static Hashtable yearCache = new Hashtable();
    public static final String[] CAN = {
            "Giap", "At", "Binh", "Dinh", "Mau", "Ky", "Canh", "Tan", "Nham", "Quy"};
    public static final String[] CANEX = {
            "Giap", "At", "At", "Binh", "Binh", "Dinh", "Dinh", "Mau", "Mau", "Ky",
            "Ky", "Canh", "Canh", "Tan", "Tan", "Nham", "Nham", "Quy", "Quy",
            "Giap"};
    public static final String[] CHI = {
            "Ty", "Suu", "Dan", "Mao", "Thin", "Ty", "Ngo", "Mui", "Than", "Dau",
            "Tuat", "Hoi"};
    public static final String[] CHIEX = {
            "Ty", "Suu", "Suu", "Dan", "Dan", "Mao", "Mao", "Thin", "Thin", "Ty",
            "Ty", "Ngo", "Ngo", "Mui", "Mui", "Than", "Than", "Dau", "Dau",
            "Tuat", "Tuat", "Hoi", "Hoi", "Ty"};

    public static String canChi(int hh, int dd, int mm, int yy, int leap) {
        int[] solar = lunar2Solar(dd, mm, yy, leap);
        double jd = universalToJD(solar[0], solar[1], solar[2]);
        String canHour = CANEX[MOD(INT(dd * 24 + hh + 12), 20) % 20];
        String chiHour = CHIEX[MOD(hh + 24, 24) % 24];
        String canNgay = CAN[MOD(INT(jd + 9.5), 10) % 10];
        String chiNgay = CHI[MOD(INT(jd + 1.5), 12) % 12];
        String canThang = CAN[MOD((yy * 12 + mm + 3), 10) % 10];
        String chiThang = CHI[MOD((mm + 1), 12) % 12];
        String nhuan = leap == 1 ? " (nhuan)" : "";
        String canNam = CAN[MOD(yy + 6, 10) % 10];
        String chiNam = CHI[MOD(yy + 8, 12) % 12];
        return "Gio " + canHour + " " + chiHour + ", Ngay " + canNgay + " "
                + chiNgay + ", thang " + canThang + " "
                + chiThang + nhuan + ", nam " + canNam + " " + chiNam;
    }

    public static String canChi(int yy, int leap) {
        String canNam = CAN[MOD(yy + 6, 10) % 10];
        String chiNam = CHI[MOD(yy + 8, 12) % 12];
        return canNam + " " + chiNam;
    }

    public static int[][] getLunarYear(int yy) {
        int[][] ret = (int[][]) yearCache.get(new Integer(yy));
        if (ret == null) {
            ret = lunarYear(yy);
            if (yearCache.size() > 10) {
                yearCache.clear();
            }
            yearCache.put(new Integer(yy), ret);
        }
        return ret;
    }

    static void initLeapYear(int[][] ret) {
        double[] sunLongitudes = new double[ret.length];
        for (int i = 0; i < ret.length; i++) {
            int[] a = ret[i];
            double jdAtMonthBegin = localToJD(a[0], a[1], a[2]);
            sunLongitudes[i] = sunLongitude(jdAtMonthBegin);
        }
        boolean found = false;
        for (int i = 0; i < ret.length; i++) {
            if (found) {
                ret[i][3] = MOD(i + 10, 12);
                continue;
            }
            double sl1 = sunLongitudes[i];
            double sl2 = sunLongitudes[i + 1];
            boolean hasMajorTerm = Math.floor(sl1 / PI * 6)
                    != Math.floor(sl2 / PI * 6);
            if (!hasMajorTerm) {
                found = true;
                ret[i][4] = 1;
                ret[i][3] = MOD(i + 10, 12);
            }
        }
    }

    public static int INT(double d) {
        return (int) Math.floor(d);
    }

    public static int[] localFromJD(double JD) {
        return universalFromJD(JD + LOCAL_TIMEZONE / 24.0);
    }

    public static double localToJD(int D, int M, int Y) {
        return universalToJD(D, M, Y) - LOCAL_TIMEZONE / 24.0;
    }

    public static int[] lunar2Solar(int D, int M, int Y, int leap) {
        int yy = Y;
        if (M >= 11) {
            yy = Y + 1;
        }
        int[][] lunarYear = getLunarYear(yy);
        int[] lunarMonth = null;
        for (int i = 0; i < lunarYear.length; i++) {
            int[] lm = lunarYear[i];
            if (lm[3] == M && lm[4] == leap) {
                lunarMonth = lm;
                break;
            }
        }
        if (lunarMonth != null) {
            double jd = localToJD(lunarMonth[0], lunarMonth[1], lunarMonth[2]);
            return localFromJD(jd + D - 1);
        } else {
            return null;
//                 throw new RuntimeException("Incorrect input!");
        }
    }

    public static int[] lunarMonth11(int Y) {
        double off = localToJD(31, 12, Y) - 2415021.076998695;
        int k = INT(off / 29.530588853);
        double jd = newMoon(k);
        int[] ret = localFromJD(jd);
        double sunLong = sunLongitude(localToJD(ret[0], ret[1], ret[2])); // sun longitude at local midnight
        if (sunLong > 3 * PI / 2) {
            jd = newMoon(k - 1);
        }
        return localFromJD(jd);
    }

    public static int[][] lunarYear(int Y) {
        int[][] ret = null;
        int[] month11A = lunarMonth11(Y - 1);
        double jdMonth11A = localToJD(month11A[0], month11A[1], month11A[2]);
        int k = (int) Math.floor(0.5
                + (jdMonth11A - 2415021.076998695)
                / 29.530588853);
        int[] month11B = lunarMonth11(Y);
        double off = localToJD(month11B[0], month11B[1], month11B[2])
                - jdMonth11A;
        boolean leap = off > 365.0;
        if (!leap) {
            ret = new int[13][5];
        } else {
            ret = new int[14][5];
        }
        ret[0] = new int[]{
                month11A[0], month11A[1], month11A[2], 0, 0};
        ret[ret.length - 1] = new int[]{
                month11B[0], month11B[1], month11B[2], 0, 0};
        for (int i = 1; i < ret.length - 1; i++) {
            double nm = newMoon(k + i);
            int[] a = localFromJD(nm);
            ret[i] = new int[]{
                    a[0], a[1], a[2], 0, 0};
        }
        for (int i = 0; i < ret.length; i++) {
            ret[i][3] = MOD(i + 11, 12);
        }
        if (leap) {
            initLeapYear(ret);
        }
        return ret;
    }

    public static int MOD(int x, int y) {
        int z = x - (int) (y * Math.floor(((double) x / y)));
        if (z == 0) {
            z = y;
        }
        return z;
    }

    public static double newMoon(int k) {
        double T = k / 1236.85; // Time in Julian centuries from 1900 January 0.5
        double T2 = T * T;
        double T3 = T2 * T;
        double dr = PI / 180;
        double Jd1 = 2415020.75933 + 29.53058868 * k + 0.0001178 * T2
                - 0.000000155 * T3;
        Jd1 = Jd1
                + 0.00033 * Math.sin((166.56 + 132.87 * T - 0.009173 * T2) * dr); // Mean new moon
        double M = 359.2242 + 29.10535608 * k - 0.0000333 * T2
                - 0.00000347 * T3; // Sun's mean anomaly
        double Mpr = 306.0253 + 385.81691806 * k + 0.0107306 * T2
                + 0.00001236 * T3; // Moon's mean anomaly
        double F = 21.2964 + 390.67050646 * k - 0.0016528 * T2
                - 0.00000239 * T3; // Moon's argument of latitude
        double C1 = (0.1734 - 0.000393 * T) * Math.sin(M * dr)
                + 0.0021 * Math.sin(2 * dr * M);
        C1 = C1 - 0.4068 * Math.sin(Mpr * dr) + 0.0161 * Math.sin(dr * 2 * Mpr);
        C1 = C1 - 0.0004 * Math.sin(dr * 3 * Mpr);
        C1 = C1 + 0.0104 * Math.sin(dr * 2 * F)
                - 0.0051 * Math.sin(dr * (M + Mpr));
        C1 = C1 - 0.0074 * Math.sin(dr * (M - Mpr))
                + 0.0004 * Math.sin(dr * (2 * F + M));
        C1 = C1 - 0.0004 * Math.sin(dr * (2 * F - M))
                - 0.0006 * Math.sin(dr * (2 * F + Mpr));
        C1 = C1 + 0.0010 * Math.sin(dr * (2 * F - Mpr))
                + 0.0005 * Math.sin(dr * (2 * Mpr + M));
        double deltat;
        if (T < -11) {
            deltat = 0.001 + 0.000839 * T + 0.0002261 * T2 - 0.00000845 * T3
                    - 0.000000081 * T * T3;
        } else {
            deltat = -0.000278 + 0.000265 * T + 0.000262 * T2;
        }

        double JdNew = Jd1 + C1 - deltat;
        return JdNew;
    }

    public static int[] solar2Lunar(int D, int M, int Y) {
        int yy = Y;
        int[][] ly = getLunarYear(Y);
        int[] month11 = ly[ly.length - 1];
        double jdToday = localToJD(D, M, Y);
        double jdMonth11 = localToJD(month11[0], month11[1], month11[2]);
        if (jdToday >= jdMonth11) {
            ly = getLunarYear(Y + 1);
            yy = Y + 1;
        }
        int i = ly.length - 1;
        while (jdToday < localToJD(ly[i][0], ly[i][1], ly[i][2])) {
            i--;
        }
        int dd = (int) (jdToday - localToJD(ly[i][0], ly[i][1], ly[i][2])) + 1;
        int mm = ly[i][3];
        if (mm >= 11) {
            yy--;
        }
        return new int[]{
                dd, mm, yy, ly[i][4]};
    }

    public static double sunLongitude(double jdn) {
        double T = (jdn - 2451545.0) / 36525; // Time in Julian centuries from 2000-01-01 12:00:00 GMT
        double T2 = T * T;
        double dr = PI / 180; // degree to radian
        double M = 357.52910 + 35999.05030 * T - 0.0001559 * T2
                - 0.00000048 * T * T2; // mean anomaly, degree
        double L0 = 280.46645 + 36000.76983 * T + 0.0003032 * T2; // mean longitude, degree
        double DL = (1.914600 - 0.004817 * T - 0.000014 * T2) * Math.sin(dr * M);
        DL = DL + (0.019993 - 0.000101 * T) * Math.sin(dr * 2 * M)
                + 0.000290 * Math.sin(dr * 3 * M);
        double L = L0 + DL; // true longitude, degree
        L = L * dr;
        L = L - PI * 2 * (INT(L / (PI * 2))); // Normalize to (0, 2*PI)
        return L;
    }

    public static int[] universalFromJD(double JD) {
        int Z, A, alpha, B, C, D, E, dd, mm, yyyy;
        double F;
        Z = INT(JD + 0.5);
        F = (JD + 0.5) - Z;
        if (Z < 2299161) {
            A = Z;
        } else {
            alpha = INT((Z - 1867216.25) / 36524.25);
            A = Z + 1 + alpha - INT(alpha / 4);
        }
        B = A + 1524;
        C = INT((B - 122.1) / 365.25);
        D = INT(365.25 * C);
        E = INT((B - D) / 30.6001);
        dd = INT(B - D - INT(30.6001 * E) + F);
        if (E < 14) {
            mm = E - 1;
        } else {
            mm = E - 13;
        }
        if (mm < 3) {
            yyyy = C - 4715;
        } else {
            yyyy = C - 4716;
        }
        return new int[]{
                dd, mm, yyyy};
    }

    public static double universalToJD(int D, int M, int Y) {
        double JD;
        if (Y > 1582 || (Y == 1582 && M > 10)
                || (Y == 1582 && M == 10 && D > 14)) {
            JD = 367 * Y - INT(7 * (Y + INT((M + 9) / 12)) / 4)
                    - INT(3 * (INT((Y + (M - 9) / 7) / 100) + 1) / 4)
                    + INT(275 * M / 9) + D + 1721028.5;
        } else {
            JD = 367 * Y - INT(7 * (Y + 5001 + INT((M - 9) / 7)) / 4)
                    + INT(275 * M / 9) + D + 1729776.5;
        }
        return JD;
    }

    public static String getStringDateMonthYear(int dd, int mm, int yyyy) {
        String dds = "";
        String mms = "";
        String yyyys = "";
        if (yyyy < 100) {
            yyyy = 1900 + yyyy;
        }
        if (dd < 10) {
            dds = "0" + dd;
        } else {
            dds = "" + dd + "";
        }
        if (mm < 10) {
            mms = "0" + mm + "";
        } else {
            mms = "" + mm + "";
        }
        return dds + "/" + mms + "/" + yyyy;
    }

    public static int processDateMonthYear(String dd, String mm, String yyyy,
                                           int AD) {
        int tampDay = 0;
        int day = 0;
        int month = 0;
        int year = 0;
        if (dd == null || dd.equals("")) {
            return 1;
        }
        if (mm == null || mm.equals("")) {
            return 2;
        }
        if (yyyy == null || yyyy.equals("")) {
            return 3;
        }
        try {
            day = Integer.parseInt(dd);
        } catch (NumberFormatException ex) {
            return 1;
        }
        if (AD == 1) {
            if (day < 0 || day > 31) {
                return 1;
            }
        } else if (AD == -1) {
            if (day < 0 || day > 30) {
                return 1;
            }
        }
        try {
            month = Integer.parseInt(mm);
        } catch (NumberFormatException ex1) {
            return 2;
        }
        if (month < 0 || month > 12) {
            return 2;
        }
        try {
            year = Integer.parseInt(yyyy);
        } catch (NumberFormatException ex2) {
            return 3;
        }
        if (year < 100) {
            year = 1900 + year;
        }
        if (year < 1900 || year > 2100) {
            return 3;
        }
        if (AD == 1) {
            System.out.println("nhay vao day");
            String monthYear = "" + month + "/" + year + "";
            tampDay = getLastestDayOfMonth(monthYear);
            if (tampDay < day) {
                return 4;
            }
        }
        return 0;
    }

    public static int getLeapYear(int year) {
        int leapyear = 0;
        if (year % 100 == 0) {
            if (year % 400 == 0) {
                leapyear = 1;
            }
        } else if ((year % 4) == 0) {
            leapyear = 1;
        }
        return leapyear;
    }

    public static int getLastestDayOfMonth(String strMonthYear) {
        String strDate = strMonthYear;
        int i, nYear, nMonth, nDay;
        String strSub = null;

        i = strDate.indexOf("/");
        if (i < 0) {
            return 0;
        }
        strSub = strDate.substring(0, i);
        nMonth = (new Integer(strSub)).intValue(); // Month begin from 0 value
        strDate = strDate.substring(i + 1);
        nYear = (new Integer(strDate)).intValue();

        boolean leapyear = false;
        if (nYear % 100 == 0) {
            if (nYear % 400 == 0) {
                leapyear = true;
            }
        } else if ((nYear % 4) == 0) {
            leapyear = true;
        }

        if (nMonth == 2) {
            if (leapyear) {
                return 29;
            } else {
                return 28;
            }
        } else {
            if ((nMonth == 1) || (nMonth == 3) || (nMonth == 5)
                    || (nMonth == 7) || (nMonth == 8) || (nMonth == 10)
                    || (nMonth == 12)) {
                return 31;
            } else if ((nMonth == 4) || (nMonth == 6) || (nMonth == 9)
                    || (nMonth == 11)) {
                return 30;
            }
        }
        return 0;
    }

    public static String getThuNgay(String Date, String Month, String Year) {
        String thungay[] = {
                "chu nhat", "thu hai", "thu ba", "thu tu", "thu nam", "thu sau",
                "thu bay"};

        Timestamp ddmmyyyy = String2Timestamp(getStringDateMonthYear(
                Integer.parseInt(Date), Integer.parseInt(Month),
                Integer.parseInt(Year)));
        int thu = getDayOfWeek(ddmmyyyy);
        return thungay[thu - 1];
    }

    //................................................
    public static String removeCharAt(String s, int pos) {
        if (s == null || pos < 0) {
            return s;
        }
        //return s.substring(0, pos) + s.substring(pos + 1);
        StringBuffer buf = new StringBuffer(s.length() - 1);
        buf.append(s.substring(0, pos)).append(s.substring(pos + 1));
        return buf.toString();
    }

    public static String replaceChar(String strMsg) {
        String sTmp = strMsg;
        sTmp = sTmp.replace('-', '/');
        sTmp = sTmp.replace('.', '/');
        sTmp = sTmp.replace(' ', '/');
        sTmp = sTmp.replace('_', '/');
        return sTmp;
    }

    /**
     * @param request: input string contains ddmm or dd mm
     * @return
     */
    public static String formatDate(String request) {
        String sDate = "";
        if (request == null || "".equals(request)) {
            return "";
        }
        if (request.length() > 5) {
            request = request.substring(0, 5);
        }
        request = replaceChar(request);
        if (request.indexOf("/") > 0 && request.length() <= 5) {
            sDate = request + "/" + getYYYY();
            return convertToDateCalendar(sDate);
        }
        if (request.length() == 2) {
            sDate = request.substring(0, 1) + "/" + request.substring(1) + "/"
                    + getYYYY();
            return convertToDateCalendar(sDate);
        } else if (request.length() == 3) {
            int first = Integer.parseInt(request.substring(0, 1));
            int mid = Integer.parseInt(request.substring(1, 2));
            if (first <= 2 || (first == 3 && mid <= 1)) {
                sDate = request.substring(0, 2) + "/" + request.substring(2) + "/"
                        + getYYYY();
                return convertToDateCalendar(sDate);
            } else if (mid <= 1) {
                sDate = request.substring(0, 1) + "/" + request.substring(1) + "/"
                        + getYYYY();
                return convertToDateCalendar(sDate);
            } else {
                return "";
            }
        } else if (request.length() == 4) {
            int iDay = Integer.parseInt(request.substring(0, 2));
            int iMonth = Integer.parseInt(request.substring(2));
            if (iDay < 1 || iDay > 31 || iMonth < 0 || iMonth > 12) {
                return "";
            } else {
                sDate = request.substring(0, 2) + "/" + request.substring(2) + "/"
                        + getYYYY();
                return convertToDateCalendar(sDate);
            }
        } else {
            return "";
        }
    }

    public static String getCurrentPartitionIndex() {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        int year = calendar.get(calendar.YEAR);
        year = (year - 2004) * 12;
        return String.valueOf(calendar.get(calendar.MONTH) + 1 + year).trim();
    }

    public static String getPartitionIndex(Timestamp ts) {
        if (ts == null) {
            ts = new Timestamp(System.currentTimeMillis());
        }
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new java.util.Date(ts.getTime()));
        int year = calendar.get(calendar.YEAR);
        year = (year - 2004) * 12;
        return String.valueOf(calendar.get(calendar.MONTH) + 1 + year).trim();
    }

    public static String getYYMM(Timestamp ts) {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyMM");
        java.util.Date currentTime_1 = new java.util.Date(ts.getTime());
        String monthYear = formatter.format(currentTime_1);
        return monthYear;
    }

    public static String getYYYYMM(Timestamp ts) {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMM");
        java.util.Date currentTime_1 = new java.util.Date(ts.getTime());
        String monthYear = formatter.format(currentTime_1);
        return monthYear;
    }

    /**
     * Ham thuc hien validate date xem gia tri ngay sinh co dung kieu dd/MM/yyyy khong
     *
     * @param strDate
     * @return
     */
    public static boolean validDate(String strDate) {
        pattern = Pattern.compile(DATE_PATTERN);
        matcher = pattern.matcher(strDate);
        if (matcher.matches()) {
            matcher.reset();
            if (matcher.find()) {
                String day = matcher.group(1);
                String month = matcher.group(2);
                int year = Integer.parseInt(matcher.group(3));
                if (day.equals("31") && (month.equals("4") || month.equals("6") || month.equals("9")
                        || month.equals("11") || month.equals("04") || month.equals("06") || month.equals("09"))) {
                    return false; // only 1,3,5,7,8,10,12 has 31 days
                } else if (month.equals("2") || month.equals("02")) {
                    //leap year
                    if (year % 4 == 0) {
                        if (day.equals("30") || day.equals("31")) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        if (day.equals("29") || day.equals("30") || day.equals("31")) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static Date truncDate(DateTime dateTime) throws Exception {
        return new Date(dateTime.withTimeAtStartOfDay().getMillis());
    }

    public static void main(String args[]) {
//        System.out.println(getDateString(createTimestamp()));
//        System.out.println(getYYMMDDHHMISS(null));
//        long time = System.currentTimeMillis();
//        System.out.println(Long.toHexString(time));
//        System.out.println(getHHMMSSBlank(new Timestamp(System.currentTimeMillis())));
//        System.out.println(getTimestamp("20131007", "yyyyMMdd"));
//        System.out.println("Current week of year" + getCurrentWeekIndexOfYear());
//        System.out.println(getSunday(createTimestamp()));
//        System.out.println(getTimestamp("20140107", "yyyyMMdd"));
        System.out.println(validDate("31/04/1788"));
    }
}
