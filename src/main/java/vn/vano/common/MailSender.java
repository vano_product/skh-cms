package vn.vano.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

public class MailSender {
    protected Logger LOG = LoggerFactory.getLogger(MailSender.class);
//    protected String host = "10.151.6.111";
//    protected String host = "10.3.12.64";
    protected String host = "email.mobifone.vn";
    protected String port = "25";
    protected String authUserName = "trieuphu";
    protected String authPassword = "trieuphu@#2468";
    protected String emailSender = "trieuphu@mobifone.vn";
    protected Properties lstMailTo;
    protected Properties lstMailCC;
    protected Properties lstMailBCC;



    public void sendHtmlEmail(String transId, String subject, String htmlContent, String[] listCC){
        LOG.info(transId + "::BEGIN::sendHtmlEmail");
        try {
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setHost(host);
            mailSender.setPort(Integer.parseInt(port));

            mailSender.setUsername(authUserName);
            mailSender.setPassword(authPassword);

            LOG.info("host=" + host + ";port=" + port);
            LOG.info("authUserName=" + authUserName);
            LOG.info("authPassword=" + authPassword);

            Properties props = mailSender.getJavaMailProperties();
//            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
//            props.put("mail.smtp.socketFactory.port", Integer.parseInt(port));
//            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.ssl.enable", "false");
            props.put("mail.debug", "true");
//            mailSender.setJavaMailProperties(props);
//            session.setDebug(true);
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, false, "UTF-8");
//            Message message = new MimeMessage(session);
            helper.setSubject(subject);
            helper.setFrom(emailSender);
            helper.setTo(emailSender);

//            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailSender));
            if (listCC.length >0) {
                helper.setCc(listCC);
            } else if (lstMailCC != null && !lstMailCC.isEmpty()) {
                for (Object emailTo : lstMailCC.values()) {
//                    message.setRecipient(Message.RecipientType.TO, new InternetAddress(String.valueOf(emailTo)));
                    helper.setCc(String.valueOf(emailTo));
                }
            }

            if (lstMailBCC != null && !lstMailBCC.isEmpty()) {
                for (Object emailBCC : lstMailBCC.values()) {
//                    message.addRecipient(Message.RecipientType.BCC, new InternetAddress(String.valueOf(emailBCC)));
                    helper.setBcc(String.valueOf(emailBCC));
                }
            }
            helper.setText(htmlContent, true);
//            message.saveChanges();

            mailSender.send(message);
//            Transport.send(message);

        } catch (Exception ex) {
            LOG.error(transId, ex);
        } finally {
            LOG.info(transId + "::END::sendHtmlEmail");
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getAuthUserName() {
        return authUserName;
    }

    public void setAuthUserName(String authUserName) {
        this.authUserName = authUserName;
    }

    public String getAuthPassword() {
        return authPassword;
    }

    public void setAuthPassword(String authPassword) {
        this.authPassword = authPassword;
    }

    public String getEmailSender() {
        return emailSender;
    }

    public void setEmailSender(String emailSender) {
        this.emailSender = emailSender;
    }

    public Properties getLstMailTo() {
        return lstMailTo;
    }

    public void setLstMailTo(Properties lstMailTo) {
        this.lstMailTo = lstMailTo;
    }

    public Properties getLstMailCC() {
        return lstMailCC;
    }

    public void setLstMailCC(Properties lstMailCC) {
        this.lstMailCC = lstMailCC;
    }

    public Properties getLstMailBCC() {
        return lstMailBCC;
    }

    public void setLstMailBCC(Properties lstMailBCC) {
        this.lstMailBCC = lstMailBCC;
    }
}
