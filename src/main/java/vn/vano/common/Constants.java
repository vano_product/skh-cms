package vn.vano.common;

import java.text.SimpleDateFormat;

public interface Constants {
    String MOBI_CARRIER = "MOBI";
    String MOBI_SERVICE_CODE = "1566";
    String VTEL_CARRIER = "VTEL";
    String VTEL_SERVICE_CODE = "8888";
    String VINA_CARRIER = "VINA";
    String VINA_SERVICE_CODE = "8888";
    String VTEL_RESULT_SUCCESS = "0";
    String VTEL_RESULT_FAIL = "1";
    Integer G_STATUS_ACTIVE = 1;
    Integer G_STATUS_DEACTIVE = 0;
    Integer G_STATUS_BLOCK = 2;
    String G_RESULT_SUCESS = "1";
    String G_RESULT_ERROR = "0";
    String G_REGEX_API = "^(?:http?:\\/\\/)?\\w.+:\\d+\\/\\w+\\/[^\\n]+";
    //String DATE_REGEX = "^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$";//chua dung den

    Integer TRANS_ID_LENGTH = 12;
    Integer TOKEN_LENGTH = 20;
    Integer MPIN_LENGTH = 4;
    Integer OTP_LENGTH = 6;

    interface Session {
        String USER_LOGIN = "userName";
        String USER_ID = "userId";
        String FULL_NAME = "fullName";
    }

    interface Table {
        int DISPLAY_10_RECORD = 10;
        int DISPLAY_20_RECORD = 20;
        int DISPLAY_50_RECORD = 50;
        int DISPLAY_100_RECORD = 100;
    }

    interface SysParam {
        String TOPUP_CSKH_MAX_MONEY = "CSKH_MAX_MONEY";
    }

    interface App {
        String SERVER_MODE = "";
    }

    interface CoreMo {
        //Cac trang thai cua ban ghi trong core_mo
        interface Status {
            String EXTEND = "0";
            String EXTEND_NAME = "EXTEND";
            String REG = "1";
            String REG_NAME = "REG";
            String CANCEL = "3";
            String CANCEL_NAME = "CANCEL";
            String CONFIRM = "2";
            String CONFIRM_NAME = "CONFIRM";
            String NORMAL_NAME = "NORMAL";
            String CONTENT_NAME = "CONTENT";

            Long ACTIVE = 1L;
        }

        interface ProcessStatus {
            Integer DEFAULT = 0;
            Integer EXECUTED = 1;
            Integer ERROR = 9;
        }

        interface Command {
            String CMD_DEFAULT = "DEFAULT";
            String CMD_CONTENT = "CONTENT";
            String CMD_ERROR = "ERROR";
        }

        interface Channel {
            String CHANNEL_SMS = "SMS";
            String CHANNEL_API = "API";
        }
    }

    interface CoreMt {
        interface Keyword {
            String SYNTAX_INVALID = "INVALID";
            String SYNTAX_MTREMIND = "MTREMIND";
            String SYNTAX_CONTENT = "CONTENT";
        }

        String DEFAULT_MESSAGE = "Số thuê bao không hợp lệ..";
    }

    interface Subscriber {
        Integer ACTIVE = 1;
        Integer WAIT_CONFIRM = 2;
        Integer DEACTIVE = 3;
    }

    interface SubscriberLog {
        String USER_SYS = "SYS";
        //Dang khi moi
        String ACTION_REGNEW = "REGNEW";
        //Active lai thong tin da co
        String ACTION_RENEW = "RENEW";
        //Gia han
        String ACTION_EXTEND = "EXTEND";
        //Huy
        String ACTION_CANCEL = "CANCEL";
        //Dang khi moi
        String ACTION_CONFIRM = "CONFIRM";
    }

    interface ChargeLog {
        Integer ACTIVE = 1;
        String ACTION_REG = "REGNEW";
        String ACTION_EXT = "EXTEND";
        String ACTION_BUY = "BUY";
        String DEFAULT_RESULT = "OK";
        String CHARGE_SUCCESS = "CPS-0000";

    }


    interface StoreCategory {
        String FREE = "FREE";
        String LASTEST = "LASTEST";
        String MOST_USED = "MOST_USED";
    }

    interface ACTION_TYPE {
        String SYNC_UPDATE_VIDEO = "SYNC_UPDATE_VIDEO";
        String SYNC_STATUS = "SYNC_STATUS";
        String SYNC_REGISTER_APP = "SYNC_REGISTER_APP";
        String SYNC_UPDATE_PROFILE = "SYNC_UPDATE_PROFILE";
        String ALL = "ALL";
        String NOTIFY = "NOTIFY";
        String CALL = "CALL";
        String VIDEO = "VIDEO";

        interface MESSAGE {
            String NOTIFY = "Thông báo";
            String REGISTER = "Đăng ký dịch vụ thành công";
            String BUY = "Mua video thành công";
        }
    }

    interface QuestionQueueStatus {

        int QUEUE = -1;//dang chờ quét
        int USING = 0;//đang được sử dụng cho KH ko dc lấy ra tiếp nữa
        int USED = 1;//đã kết thúc sử dụng ,xóa queue chuyển về log

    }

    interface QuestionQueue {

        int NO_PACKAGE = 0;
        int MEKE_PACKAGE = 1;
        int MEDIUM_PACKAGE = 2;
        int ALL_PACKAGE = MEDIUM_PACKAGE + MEKE_PACKAGE;

        int M_PACKAGE = 1;//machine learning
        int P_PACKAGE = 2;//promotion
        int M_ALL_PACKAGE = M_PACKAGE + P_PACKAGE;
    }

    interface COMMAND_CODE {
        //cu phap goi MEDIUM
        String M = "M";//dung chung Mobifone ,viettel
        String M1 = "M1";
        //cu phap goi MEKECONGNGHE
        String ME = "ME";
        String KE = "KE";
        //goi Mobi phone
        String P = "P";
    }

    interface TELCO {

        String VIETTEL = "VTL";
        String MOBFIONE = "VMS";

    }

    String NOT_FOUND = "NOT_FOUND";
    String NOT_FOUND_MESSAGE = "Không tìm thấy dữ liệu";

    public interface CDR {
        public static SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmss");

        public interface Status {
            public String FAIL = "0";
            public String SUCCESS = "1";
        }

        public String LAST_EXPORT_DATE_KEY = "_LAST_EXPORT_DATE_KEY_";
        public String CURR_FILE_INDEX_KEY = "_CURR_FILE_INDEX_";
        public String CURR_FILE_SIZE_KEY = "_CURR_FILE_SIZE_";
        // BigPromos
        public String LAST_EXPORT_BIGPROMOS_DATE_KEY = "_LAST_EXPORT_BIGPROMOS_DATE_KEY_";

        public interface AccountTypes {
            public String MAIN = "MAIN";
            public String KM3 = "KM3";
            public String KM2 = "KM2";
        }
    }
}
