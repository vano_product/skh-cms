package vn.vano.jpa;

import vn.vano.annotation.AntColumn;
import vn.vano.annotation.AntTable;

import java.io.Serializable;

@AntTable(name = "request_logs_temp", key = "")
public class RequestLogsTT implements Serializable {
    String msisdn;
    String linkEeq;
    String actionDate;

//    amount_total

    @AntColumn(name = "msisdn", index = 0)
    public String getMsisdn() {
        return msisdn;
    }
    @AntColumn(name = "msisdn", index = 0)
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @AntColumn(name = "link_req", index = 1)
    public String getLinkEeq() {
        return linkEeq;
    }
    @AntColumn(name = "link_req", index = 1)
    public void setLinkEeq(String linkEeq) {
        this.linkEeq = linkEeq;
    }

    @AntColumn(name = "action_date", index = 2)
    public String getActionDate() {
        return actionDate;
    }
    @AntColumn(name = "action_date", index = 2)
    public void setActionDate(String actionDate) {
        this.actionDate = actionDate;
    }





}
