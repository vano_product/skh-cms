package vn.vano.jpa;


import vn.vano.annotation.AntColumn;
import vn.vano.annotation.AntTable;

import java.io.Serializable;

@AntTable(name = "", key = "")
public class QuantityWapBean implements Serializable {

    private String dateReport;
    private Long numRegAll =0L;
    private Long numRegApi =0L;
    private Long numRegCskh =0L;
    private Long numRegIvr =0L;
    private Long numRegSms =0L;
    private Long numRegUssd =0L;
    private Long numRegWap =0L;
    private Long numRegWeb =0L;
    private Long numHuyApi =0L;
    private Long numHuyCskh =0L;
    private Long numHuyIvr =0L;
    private Long numHuySms =0L;
    private Long numHuyUssd =0L;
    private Long numHuyWap =0L;
    private Long numHuyWeb =0L;
    private Long numHuy;

    @AntColumn(name = "DATE_REPORT", index = 0)
    public String getDateReport() {
        return dateReport;
    }
    @AntColumn(name = "DATE_REPORT", index = 0)
    public void setDateReport(String dateReport) {
        this.dateReport = dateReport;
    }

    @AntColumn(name = "num_reg", index = 1)
    public Long getNumRegAll() {
        return numRegAll;
    }
    @AntColumn(name = "num_reg", index = 1)
    public void setNumRegAll(Long numRegAll) {
        this.numRegAll = numRegAll;
    }

    @AntColumn(name = "num_reg_api", index = 2)
    public Long getNumRegApi() {
        return numRegApi;
    }
    @AntColumn(name = "num_reg_api", index = 2)
    public void setNumRegApi(Long numRegApi) {
        this.numRegApi = numRegApi;
    }


    @AntColumn(name = "num_reg_cskh", index = 3)
    public Long getNumRegCskh() {
        return numRegCskh;
    }
    @AntColumn(name = "num_reg_cskh", index = 3)
    public void setNumRegCskh(Long numRegCskh) {
        this.numRegCskh = numRegCskh;
    }


    @AntColumn(name = "num_reg_ivr", index = 4)
    public Long getNumRegIvr() {
        return numRegIvr;
    }
    @AntColumn(name = "num_reg_ivr", index = 4)
    public void setNumRegIvr(Long numRegIvr) {
        this.numRegIvr = numRegIvr;
    }


    @AntColumn(name = "num_reg_sms", index = 5)
    public Long getNumRegSms() {
        return numRegSms;
    }
    @AntColumn(name = "num_reg_sms", index = 5)
    public void setNumRegSms(Long numRegSms) {
        this.numRegSms = numRegSms;
    }

    @AntColumn(name = "num_reg_ussd", index = 6)
    public Long getNumRegUssd() {
        return numRegUssd;
    }
    @AntColumn(name = "num_reg_ussd", index = 6)
    public void setNumRegUssd(Long numRegUssd) {
        this.numRegUssd = numRegUssd;
    }

    @AntColumn(name = "num_reg_wap", index = 7)
    public Long getNumRegWap() {
        return numRegWap;
    }
    @AntColumn(name = "num_reg_wap", index = 7)
    public void setNumRegWap(Long numRegWap) {
        this.numRegWap = numRegWap;
    }


    @AntColumn(name = "num_reg_web", index = 8)
    public Long getNumRegWeb() {
        return numRegWeb;
    }
    @AntColumn(name = "num_reg_web", index = 8)
    public void setNumRegWeb(Long numRegWeb) {
        this.numRegWeb = numRegWeb;
    }

    @AntColumn(name = "num_huy_api", index = 9)
    public Long getNumHuyApi() {
        return numHuyApi;
    }
    @AntColumn(name = "num_huy_api", index = 9)
    public void setNumHuyApi(Long numHuyApi) {
        this.numHuyApi = numHuyApi;
    }

    @AntColumn(name = "num_huy_cskh", index = 10)
    public Long getNumHuyCskh() {
        return numHuyCskh;
    }
    @AntColumn(name = "num_huy_cskh", index = 10)
    public void setNumHuyCskh(Long numHuyCskh) {
        this.numHuyCskh = numHuyCskh;
    }

    @AntColumn(name = "num_huy_ivr", index = 11)
    public Long getNumHuyIvr() {
        return numHuyIvr;
    }
    @AntColumn(name = "num_huy_ivr", index = 11)
    public void setNumHuyIvr(Long numHuyIvr) {
        this.numHuyIvr = numHuyIvr;
    }

    @AntColumn(name = "num_huy_sms", index = 12)
    public Long getNumHuySms() {
        return numHuySms;
    }
    @AntColumn(name = "num_huy_sms", index = 12)
    public void setNumHuySms(Long numHuySms) {
        this.numHuySms = numHuySms;
    }

    @AntColumn(name = "num_huy_ussd", index = 13)
    public Long getNumHuyUssd() {
        return numHuyUssd;
    }
    @AntColumn(name = "num_huy_ussd", index = 13)
    public void setNumHuyUssd(Long numHuyUssd) {
        this.numHuyUssd = numHuyUssd;
    }

    @AntColumn(name = "num_huy_wap", index = 14)
    public Long getNumHuyWap() {
        return numHuyWap;
    }
    @AntColumn(name = "num_huy_wap", index = 14)
    public void setNumHuyWap(Long numHuyWap) {
        this.numHuyWap = numHuyWap;
    }

    @AntColumn(name = "num_huy_web", index = 15)
    public Long getNumHuyWeb() {
        return numHuyWeb;
    }
    @AntColumn(name = "num_huy_web", index = 15)
    public void setNumHuyWeb(Long numHuyWeb) {
        this.numHuyWeb = numHuyWeb;
    }

    @AntColumn(name = "num_huy", index = 16)
    public Long getNumHuy() {
        return numHuy;
    }
    @AntColumn(name = "num_huy", index = 16)
    public void setNumHuy(Long numHuy) {
        this.numHuy = numHuy;
    }


    private String fromDateStr;
    private String toDateStr;
    public String getFromDateStr() {
        return fromDateStr;
    }

    public void setFromDateStr(String fromDateStr) {
        this.fromDateStr = fromDateStr;
    }

    public String getToDateStr() {
        return toDateStr;
    }

    public void setToDateStr(String toDateStr) {
        this.toDateStr = toDateStr;
    }

}
