package vn.vano.jpa;

import vn.vano.annotation.AntColumn;
import vn.vano.annotation.AntTable;

import java.io.Serializable;

@AntTable(name = "", key = "id")
public class QuantityDayBean implements Serializable {



    private String dateReport;
    private Long numRegnew;
    private Long numRenew;
    private Long numRegAll;
    private Long numRegApi;
    private Long numRegCskh;
    private Long numRegIvr;
    private Long numRegSms;
    private Long numRegUssd;
    private Long numRegWap;
    private Long numRegWeb;
    private Long numHuyApi;
    private Long numHuyCskh;
    private Long numHuyIvr;
    private Long numHuySms;
    private Long numHuyUssd;
    private Long numHuyWap;
    private Long numHuyWeb;
    private Long numHuy;
    private Long  numActive;
    private Long numGh;
    private String numRate;



    @AntColumn(name = "DATE_REPORT", index = 0)
    public String getDateReport() {
        return dateReport;
    }
    @AntColumn(name = "DATE_REPORT", index = 0)
    public void setDateReport(String dateReport) {
        this.dateReport = dateReport;
    }

    @AntColumn(name = "num_regnew", index = 1)
    public Long getNumRegnew() {
        return numRegnew;
    }
    @AntColumn(name = "num_regnew", index = 1)
    public void setNumRegnew(Long numRegnew) {
        this.numRegnew = numRegnew;
    }

    @AntColumn(name = "num_renew", index = 2)
    public Long getNumRenew() {
        return numRenew;
    }
    @AntColumn(name = "num_renew", index = 2)
    public void setNumRenew(Long numRenew) {
        this.numRenew = numRenew;
    }


    @AntColumn(name = "num_dk", index = 3)
    public Long getNumRegAll() {
        return numRegAll;
    }
    @AntColumn(name = "num_dk", index = 3)
    public void setNumRegAll(Long numRegAll) {
        this.numRegAll = numRegAll;
    }


    @AntColumn(name = "num_reg_api", index = 4)
    public Long getNumRegApi() {
        return numRegApi;
    }
    @AntColumn(name = "num_reg_api", index = 4)
    public void setNumRegApi(Long numRegApi) {
        this.numRegApi = numRegApi;
    }


    @AntColumn(name = "num_reg_cskh", index = 5)
    public Long getNumRegCskh() {
        return numRegCskh;
    }
    @AntColumn(name = "num_reg_cskh", index = 5)
    public void setNumRegCskh(Long numRegCskh) {
        this.numRegCskh = numRegCskh;
    }


    @AntColumn(name = "num_reg_ivr", index = 6)
    public Long getNumRegIvr() {
        return numRegIvr;
    }
    @AntColumn(name = "num_reg_ivr", index = 6)
    public void setNumRegIvr(Long numRegIvr) {
        this.numRegIvr = numRegIvr;
    }


    @AntColumn(name = "num_reg_sms", index = 7)
    public Long getNumRegSms() {
        return numRegSms;
    }
    @AntColumn(name = "num_reg_sms", index = 7)
    public void setNumRegSms(Long numRegSms) {
        this.numRegSms = numRegSms;
    }

    @AntColumn(name = "num_reg_ussd", index = 8)
    public Long getNumRegUssd() {
        return numRegUssd;
    }
    @AntColumn(name = "num_reg_ussd", index = 8)
    public void setNumRegUssd(Long numRegUssd) {
        this.numRegUssd = numRegUssd;
    }

    @AntColumn(name = "num_reg_wap", index = 9)
    public Long getNumRegWap() {
        return numRegWap;
    }
    @AntColumn(name = "num_reg_wap", index = 9)
    public void setNumRegWap(Long numRegWap) {
        this.numRegWap = numRegWap;
    }


    @AntColumn(name = "num_reg_web", index = 10)
    public Long getNumRegWeb() {
        return numRegWeb;
    }
    @AntColumn(name = "num_reg_web", index = 10)
    public void setNumRegWeb(Long numRegWeb) {
        this.numRegWeb = numRegWeb;
    }

    @AntColumn(name = "num_huy_api", index = 11)
    public Long getNumHuyApi() {
        return numHuyApi;
    }
    @AntColumn(name = "num_huy_api", index = 11)
    public void setNumHuyApi(Long numHuyApi) {
        this.numHuyApi = numHuyApi;
    }

    @AntColumn(name = "num_huy_cskh", index = 12)
    public Long getNumHuyCskh() {
        return numHuyCskh;
    }
    @AntColumn(name = "num_huy_cskh", index = 12)
    public void setNumHuyCskh(Long numHuyCskh) {
        this.numHuyCskh = numHuyCskh;
    }

    @AntColumn(name = "num_huy_ivr", index = 14)
    public Long getNumHuyIvr() {
        return numHuyIvr;
    }
    @AntColumn(name = "num_huy_ivr", index = 14)
    public void setNumHuyIvr(Long numHuyIvr) {
        this.numHuyIvr = numHuyIvr;
    }

    @AntColumn(name = "num_huy_sms", index = 15)
    public Long getNumHuySms() {
        return numHuySms;
    }
    @AntColumn(name = "num_huy_sms", index = 15)
    public void setNumHuySms(Long numHuySms) {
        this.numHuySms = numHuySms;
    }


    //    sum(num_huy_api) num_huy_api,sum(num_huy_cskh) num_huy_cskh,\n" +
// sum(num_huy_wap) num_huy_wap,sum(num_huy_web) num_huy_web,
//            sum(num_huy ) num_huy,\n" +"sum(num_active) num_active, sum(num_gh) num_gh,  ROUND(100*SUM(num_gh)/SUM(num_active),2) num_rate" +
    @AntColumn(name = "num_huy_ussd", index = 16)
    public Long getNumHuyUssd() {
        return numHuyUssd;
    }
    @AntColumn(name = "num_huy_ussd", index = 16)
    public void setNumHuyUssd(Long numHuyUssd) {
        this.numHuyUssd = numHuyUssd;
    }

    @AntColumn(name = "num_huy_wap", index = 17)
    public Long getNumHuyWap() {
        return numHuyWap;
    }
    @AntColumn(name = "num_huy_wap", index = 17)
    public void setNumHuyWap(Long numHuyWap) {
        this.numHuyWap = numHuyWap;
    }

    @AntColumn(name = "num_huy_web", index = 18)
    public Long getNumHuyWeb() {
        return numHuyWeb;
    }
    @AntColumn(name = "num_huy_web", index = 18)
    public void setNumHuyWeb(Long numHuyWeb) {
        this.numHuyWeb = numHuyWeb;
    }

    @AntColumn(name = "num_huy", index = 18)
    public Long getNumHuy() {
        return numHuy;
    }
    @AntColumn(name = "num_huy", index = 18)
    public void setNumHuy(Long numHuy) {
        this.numHuy = numHuy;
    }
    @AntColumn(name = "num_active", index = 19)
    public Long getNumActive() {
        return numActive;
    }
    @AntColumn(name = "num_active", index = 19)
    public void setNumActive(Long numActive) {
        this.numActive = numActive;
    }

    @AntColumn(name = "num_gh", index = 20)
    public Long getNumGh() {
        return numGh;
    }
    @AntColumn(name = "num_gh", index = 20)
    public void setNumGh(Long numGh) {
        this.numGh = numGh;
    }

    @AntColumn(name = "num_rate", index = 21)
    public String getNumRate() {
        return numRate;
    }
    @AntColumn(name = "num_rate", index = 21)
    public void setNumRate(String numRate) {
        this.numRate = numRate;
    }





    private String fromDateStr;
    private String toDateStr;
    public String getFromDateStr() {
        return fromDateStr;
    }

    public void setFromDateStr(String fromDateStr) {
        this.fromDateStr = fromDateStr;
    }

    public String getToDateStr() {
        return toDateStr;
    }

    public void setToDateStr(String toDateStr) {
        this.toDateStr = toDateStr;
    }
}
