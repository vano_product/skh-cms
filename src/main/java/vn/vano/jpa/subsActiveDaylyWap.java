package vn.vano.jpa;


import vn.vano.annotation.AntColumn;
import vn.vano.annotation.AntTable;

import java.io.Serializable;
import java.sql.Timestamp;


@AntTable(name = "subs_active_dayly_wap", key = "id")
public class subsActiveDaylyWap  implements Serializable {
    private Long id;
    private String dateKey; //date_key
    private String msisdn;
    private Timestamp createdTime;
    private String channel;


    @AntColumn(name = "id", auto_increment = true, index = 0)
    public Long getId() {
        return id;
    }
    @AntColumn(name = "id", auto_increment = true, index = 0)
    public void setId(Long id) {
        this.id = id;
    }

    @AntColumn(name = "date_key", index = 1)
    public String getDateKey() {
        return dateKey;
    }
    @AntColumn(name = "date_key", index = 1)
    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }

    @AntColumn(name = "msisdn", index = 2)
    public String getMsisdn() {
        return msisdn;
    }
    @AntColumn(name = "msisdn", index = 2)
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @AntColumn(name = "created_time", index = 3)
    public Timestamp getCreatedTime() {
        return createdTime;
    }
    @AntColumn(name = "created_time", index = 3)
    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    @AntColumn(name = "channel", index = 4)
    public String getChannel() {
        return channel;
    }
    @AntColumn(name = "channel", index = 4)
    public void setChannel(String channel) {
        this.channel = channel;
    }



}

