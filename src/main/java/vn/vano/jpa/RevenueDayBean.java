package vn.vano.jpa;

import vn.vano.annotation.AntColumn;
import vn.vano.annotation.AntTable;

import java.io.Serializable;

@AntTable(name = "report_wap_noturl", key = "id")
public class RevenueDayBean implements Serializable {

//    private Long id;
    private String dateReport;
    private Long amountDk;
    private Long amountRenew;

    private Long amountTotal;


    @AntColumn(name = "date_report", index = 0)
    public String getDateReport() {
        return dateReport;
    }

    @AntColumn(name = "date_report", index = 0)
    public void setDateReport(String dateReport) {
        this.dateReport = dateReport;
    }

    @AntColumn(name = "amount_dk", index = 1)
    public Long getAmountDk() {
        return amountDk;
    }

    @AntColumn(name = "amount_dk", index = 1)
    public void setAmountDk(Long amountDk) {
        this.amountDk = amountDk;
    }

    @AntColumn(name = "amount_renew", index = 2)
    public Long getAmountRenew() {
        return amountRenew;
    }

    @AntColumn(name = "amount_renew", index = 2)
    public void setAmountRenew(Long amountRenew) {
        this.amountRenew = amountRenew;
    }


    @AntColumn(name = "amount_total", index = 3)
    public Long getAmountTotal() {
        return amountTotal;
    }
    @AntColumn(name = "amount_total", index = 3)
    public void setAmountTotal(Long amountTotal) {
        this.amountTotal = amountTotal;
    }




    private String fromDateStr;
    private String toDateStr;
    public String getFromDateStr() {
        return fromDateStr;
    }

    public void setFromDateStr(String fromDateStr) {
        this.fromDateStr = fromDateStr;
    }

    public String getToDateStr() {
        return toDateStr;
    }

    public void setToDateStr(String toDateStr) {
        this.toDateStr = toDateStr;
    }



}
