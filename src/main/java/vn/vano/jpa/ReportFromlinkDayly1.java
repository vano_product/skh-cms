package vn.vano.jpa;


import vn.vano.annotation.AntColumn;
import vn.vano.annotation.AntTable;
import vn.vano.bean.LinkRegBean;

import java.io.Serializable;
import java.util.List;

@AntTable(name = "", key = "")
public class ReportFromlinkDayly1 implements Serializable {
    private String dateReport;
    private String numReg;
    private String numCancel;
    private String active;
    private String numGh;
    private String amountGh;
    private String amountTotal;
    private String numRate;


    @AntColumn(name = "date_report", index = 0)
    public String getDateReport() {
        return dateReport;
    }
    @AntColumn(name = "date_report", index = 0)
    public void setDateReport(String dateReport) {
        this.dateReport = dateReport;
    }


    @AntColumn(name = "num_reg", index = 1)
    public String getNumReg() {
        return numReg;
    }
    @AntColumn(name = "num_reg", index = 1)
    public void setNumReg(String numReg) {
        this.numReg = numReg;
    }

    @AntColumn(name = "num_cancel", index = 2)
    public String getNumCancel() {
        return numCancel;
    }
    @AntColumn(name = "num_cancel", index = 2)
    public void setNumCancel(String numCancel) {
        this.numCancel = numCancel;
    }

    @AntColumn(name = "active", index = 3)
    public String getActive() {
        return active;
    }
    @AntColumn(name = "active", index = 3)
    public void setActive(String active) {
        this.active = active;
    }

    @AntColumn(name = "num_gh", index = 4)
    public String getNumGh() {
        return numGh;
    }
    @AntColumn(name = "num_gh", index = 4)
    public void setNumGh(String numGh) {
        this.numGh = numGh;
    }

    @AntColumn(name = "amount_gh", index = 5)
    public String getAmountGh() {
        return amountGh;
    }
    @AntColumn(name = "amount_gh", index = 5)
    public void setAmountGh(String amountGh) {
        this.amountGh = amountGh;
    }

    @AntColumn(name = "amount_total", index = 6)
    public String getAmountTotal() {
        return amountTotal;
    }
    @AntColumn(name = "amount_total", index = 6)
    public void setAmountTotal(String amountTotal) {
        this.amountTotal = amountTotal;
    }


    @AntColumn(name = "num_rate", index = 7)
    public String getNumRate() {
        return numRate;
    }
    @AntColumn(name = "num_rate", index = 7)
    public void setNumRate(String numRate) {
        this.numRate = numRate;
    }


    private String fromDateStr;
    private String toDateStr;
    public String getFromDateStr() {
        return fromDateStr;
    }

    public void setFromDateStr(String fromDateStr) {
        this.fromDateStr = fromDateStr;
    }

    public String getToDateStr() {
        return toDateStr;
    }

    public void setToDateStr(String toDateStr) {
        this.toDateStr = toDateStr;
    }

    public String getLinkReqTT() {
        return linkReqTT;
    }

    public void setLinkReqTT(String linkReqTT) {
        this.linkReqTT = linkReqTT;
    }

    public  String  linkReqTT;



}
