package vn.vano.jpa;

import vn.vano.annotation.AntColumn;
import vn.vano.annotation.AntTable;

import java.io.Serializable;

@AntTable(name = "", key = "")

public class QuantityWapChanelBean implements Serializable {
    private String dateReport;
    private Long numRegAll =0L;

    @AntColumn(name = "DATE_REPORT", index = 0)
    public String getDateReport() {
        return dateReport;
    }
    @AntColumn(name = "DATE_REPORT", index = 0)
    public void setDateReport(String dateReport) {
        this.dateReport = dateReport;
    }

    @AntColumn(name = "num_reg", index = 1)
    public Long getNumRegAll() {
        return numRegAll;
    }
    @AntColumn(name = "num_reg", index = 1)
    public void setNumRegAll(Long numRegAll) {
        this.numRegAll = numRegAll;
    }



    private String fromDateStr;
    private String toDateStr;
    public String getFromDateStr() {
        return fromDateStr;
    }

    public void setFromDateStr(String fromDateStr) {
        this.fromDateStr = fromDateStr;
    }

    public String getToDateStr() {
        return toDateStr;
    }

    public void setToDateStr(String toDateStr) {
        this.toDateStr = toDateStr;
    }
}
